# -*- coding: utf-8 -*-
"""
Created on Mon Dec 18 11:50:01 2023

@author: emidioho
"""

import time
import pandas as pd

import numpy as np
import geopandas as gpd
import xarray as xr

import contextily as ctx

import seaborn as sns
from matplotlib import pyplot as plt
from matplotlib.colors import Normalize
plt.style.use('seaborn-v0_8-ticks') #plt.style.available[24], ggplot, seaborn-v0_8-whitegrid, seaborn-v0_8-ticks
from GN_functions import get_super

# from matplotlib.colors import LinearSegmentedColormap

#%%
figure_path = r"C:\Users\emidioho\OneDrive - Stichting Deltares\Documents\Projects\Bridge\figures"
folder_path = r"C:\Users\emidioho\OneDrive - Stichting Deltares\Documents\Projects\Bridge"
#### read shapefiles 
data_path = r"C:\Users\emidioho\OneDrive - Stichting Deltares\Documents\Projects\Bridge"
data_shape = gpd.read_file(r'N:\Projects\1230000\1230409\B. Measurements and calculations\26.dashboarddata\chennai\shp\areas.shp')   
data_shape = data_shape.drop(data_shape.index[7])
data_shape = data_shape.reset_index()

data_shape['coords'] = data_shape['geometry'].apply(lambda x: x.representative_point().coords[:])
data_shape['coords'] = [coords[0] for coords in data_shape['coords']]

names = data_shape.name

scenario = 'SSP3'

## read calculated gap
gap = pd.read_csv(f'{folder_path}\water_gap_Chennai_{scenario}.csv')
gap.drop(gap.columns[[0,1]], axis=1, inplace=True)

#### read wgi_none_SSPx.csv   (this is yearly/decanal based)
WGI_chennai_dec = pd.read_csv(r'N:\Projects\1230000\1230409\B. Measurements and calculations\26.dashboarddata\chennai\csv\wgi_none_SSP3_none.csv')
# gN = pd.read_csv(r'N:\Projects\1230000\1230409\B. Measurements and calculations\Risk Calculations\water\gap_SSP3.csv')
# WG_data = WG.drop(columns=['id'])


## or
WG = gap
water_gap_dict = { 'Municip': [],
                  'median_WG': [],
                  'WG_data': [],
                  }


gaps_stat = []
### compute the number of water_gap ocurrences
for i in range(np.size(WG,0)):
    
    data = np.array(WG.iloc[i][1:])
    # data_a = data.reshape(int(np.size(data)/12),12)
    gaps_count = sum(np.where(data>0,1,0))
    
    water_gap_dict['Municip'].extend([int(WG.iloc[i][0])] )
    water_gap_dict['median_WG'].extend( [np.median(data)] )
    water_gap_dict['WG_data'].extend( [data] )
    
    if i==0:
        gaps_stat = gaps_count
    else:
        gaps_stat = np.vstack((gaps_stat, gaps_count))

# ### merging WG back to shapefile
# # data_shape = data_shape.assign(month_risk = water_gap_dict['WG_max_month'])
# data_shape = data_shape.assign(median_WG = water_gap_dict['median_WG'])


####################################################################################################################################
#######################################################  data array analysis  ######################################################
####################################################################################################################################
#%% DataArray preparation
a = np.empty((np.size(gap,0),12))
a[:] = np.nan
x = np.linspace(0,11,12)
y = np.linspace(0,np.size(gap,0)-1,np.size(gap,0))

time_year = pd.date_range('1/1/2000', periods=59, freq='Y')
# # start of each year: 
y_start =[]
for i in range(np.size(gap,1)):
    if time.strptime(gap.columns[i],'%Y-%m-%d').tm_mon == 12:
        y_start.append(int(i))



for i in range(np.size(time_year,0)):
    
    data_year = np.array( gap[gap.columns[int(y_start[i]):int(y_start[i+1])] ] )  

    if i==0:
        da = xr.DataArray(data_year)
        da=da.expand_dims(time = [time_year[i]])
    if i == 19 or 39:
        data_year = np.nanmean( [ gap[gap.columns[int(y_start[i])-1]], gap[gap.columns[int(y_start[i])+1]] ],0 )
        data_year = xr.DataArray(data_year)
        data_year = data_year.expand_dims(time = [time_year[i]])
        da= xr.concat([da, data_year],dim='time')
            
    else:
        data_year = xr.DataArray(data_year)
        data_year = data_year.expand_dims(time = [time_year[i]])
        
        da= xr.concat([da, data_year],dim='time')


da = da.rename({'dim_0' : 'location','dim_1':'month'})
da = da.assign_attrs({'location_name':names })



da_sel = da.where(da.values>0,np.nan)
da_sel = da_sel.where(np.isnan(da_sel.values),1)
da_sel_sum = da_sel.sel(time=slice(da_sel.time[0],da_sel.time[-1])).sum('time')


y_00_10_n = np.array(da_sel.sel(time=slice(da.time[0],da.time[11])).sum('time')) 
y_10_20_n = np.array(da_sel.sel(time=slice(da.time[12],da.time[21])).sum('time')) 
y_20_30_n = np.array(da_sel.sel(time=slice(da.time[22],da.time[31])).sum('time'))
y_30_40_n = np.array(da_sel.sel(time=slice(da.time[32],da.time[41])).sum('time'))
y_40_50_n = np.array(da_sel.sel(time=slice(da.time[42],da.time[51])).sum('time'))
y_50_60_n = np.array(da_sel.sel(time=slice(da.time[52],da.time[-1])).sum('time'))

### merging years_meanWG to shapefile
data_shape = data_shape.assign(y_00_10 = np.array(np.mean(da.sel(time=slice(da.time[0],da.time[11])).mean('time'),1)) )
data_shape = data_shape.assign(y_10_20 = np.array(np.mean(da.sel(time=slice(da.time[12],da.time[21])).mean('time'),1)) )
data_shape = data_shape.assign(y_20_30 = np.array(np.mean(da.sel(time=slice(da.time[22],da.time[31])).mean('time'),1)) )
data_shape = data_shape.assign(y_30_40 = np.array(np.mean(da.sel(time=slice(da.time[32],da.time[41])).mean('time'),1)) )
data_shape = data_shape.assign(y_40_50 = np.array(np.mean(da.sel(time=slice(da.time[42],da.time[51])).mean('time'),1)) )
data_shape = data_shape.assign(y_50_60 = np.array(np.mean(da.sel(time=slice(da.time[52],da.time[-1])).mean('time'),1)) )

### merging counts of years_WG to shapefile
data_shape = data_shape.assign(y_00_10_n = np.array(np.nansum(da_sel.sel(time=slice(da_sel.time[0],da_sel.time[11])).sum('time'),1)) )
data_shape = data_shape.assign(y_10_20_n = np.array(np.nansum(da_sel.sel(time=slice(da_sel.time[12],da_sel.time[21])).sum('time'),1)) )
data_shape = data_shape.assign(y_20_30_n = np.array(np.nansum(da_sel.sel(time=slice(da_sel.time[22],da_sel.time[31])).sum('time'),1)) )
data_shape = data_shape.assign(y_30_40_n = np.array(np.nansum(da_sel.sel(time=slice(da_sel.time[32],da_sel.time[41])).sum('time'),1)) )
data_shape = data_shape.assign(y_40_50_n = np.array(np.nansum(da_sel.sel(time=slice(da_sel.time[42],da_sel.time[51])).sum('time'),1)) )
data_shape = data_shape.assign(y_50_60_n = np.array(np.nansum(da_sel.sel(time=slice(da_sel.time[52],da_sel.time[-1])).sum('time'),1)) )



norm_da = da.where(da.values>0,np.nan)
# for i in range(np.size(time_year,0)):
norm_da = norm_da/norm_da.max('time')
norm_da = norm_da.where(norm_da.values>0,0)

#%% figure elements
months = np.linspace(1,12,12)
months_lbl = ('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dez')
cmap = 'RdYlGn_r'
cmap_months = 'twilight'
names_loc = np.linspace(0+0.5,len(gaps_stat)-0.5,len(gaps_stat))
alpha = 0.9

annot_kws = {'color':'k', 'size':9}


#%% total monthly water gap FIGURE
def fig_2():
    fig, ax = plt.subplots(figsize=(9, 9), num=2, clear=True)
    plt.subplots_adjust(left=0.25, right=0.9, bottom=0.05, top=0.95, wspace=0.2, hspace=0.3)
    
    
    sns.heatmap(da_sel_sum, cmap=cmap, cbar=False, linewidths=0.003, linecolor='k', alpha=alpha, annot=True, annot_kws=annot_kws)
    
    cbar = ax.figure.colorbar(ax.collections[0], ticks=np.linspace(0,np.max(da_sel_sum),7).astype(int),
                              shrink = 1, aspect= 30, pad = 0.1, alpha=alpha)
    cbar.set_label('total monthly water gaps \n(between 2000 and 2060)', labelpad=10)
    
    plt.xticks(months)
    plt.xticks(months-0.5, labels = months_lbl)
    plt.yticks(names_loc ,labels = names, rotation=0)
    # plt.xlabel('month')
    plt.title('frequency a month presents water gap (between 2000-2060)')
    return fig
fig = fig_2()
# plt.savefig(f"{figure_path}\\Chennai_frequency_month_WG_{scenario}.png", dpi=200)


#%% MAP
# norm = Normalize(1, 12)
# def fig_3():
#     fig, ax = plt.subplots(figsize=(12,5), num=3, clear=True)
    
#     data_shape.plot(column = 'month_risk',  edgecolor='k', linewidth=0.25, norm=norm, cmap=cmap_months, legend=False, ax=ax)
#     for idx, row in data_shape.iterrows():
#         plt.annotate(text=row['tot_WG'], xy=row['coords'], horizontalalignment='center',size=7)
        
#     ctx.add_basemap(ax, zoom=9, crs='EPSG:4326', alpha=0.7, source="CartoDB.Positron") ## add a basemap ("CartoDB.Voyager", "CartoDB.Positron")
    
#     cbar = plt.cm.ScalarMappable(norm=norm, cmap=cmap_months)
#     ax_cbar = fig.colorbar(cbar, ax=ax)  # add colorbar 
#     ax_cbar.set_ticks([1,2,3,4,5,6,7,8,9,10,11,12], labels = months_lbl)
#     ax_cbar.set_label('month with more water gaps\n (2000-2040)', labelpad=10)   # add label for the colorbar
    
#     plt.xlabel(f"Longitude [{get_super(' o')}]", labelpad=10.0)
#     plt.ylabel(f"Latitude [{get_super(' o')}]", labelpad=10.0)
#     return fig

# fig = fig_3()
# plt.savefig(f"{figure_path}\\Chennai_month_more_WG_{scenario}.png", dpi=200)


#%%  Fig 1
# width = 0.1
# fig, ax = plt.subplots(1,1,num=1, clear=True)
# plt.subplots_adjust(left=0.1, right=0.9, bottom=0.15, top=0.9, wspace=0.2, hspace=0.3)

# # ax1 = plt.subplot(211)
# # for i in range(np.size(water_gap_dict['WG_data_a'],0)):
# #     if i in np.int16(np.linspace(1,39,20)):
# #         for ii in range(np.size(water_gap_dict['WG_data_a'][i],0)):
# #             if ii<11:        
# #                 plt.bar(months,water_gap_dict['WG_data_a'][i][ii], alpha=1, linewidth=0.1, edgecolor='g',width=width)
# #             elif ii>10 and ii<21:        
# #                 plt.bar(months +ii*0.02,water_gap_dict['WG_data_a'][i][ii], alpha=1, linewidth=0.1, edgecolor='y', width=width)
# #             elif ii>20 and ii<31:        
# #                 plt.bar(months +ii*0.02,water_gap_dict['WG_data_a'][i][ii], alpha=1, linewidth=0.1, edgecolor='orange', width=width)  
# #             elif ii>30:        
# #                 plt.bar(months +ii*0.02,water_gap_dict['WG_data_a'][i][ii], alpha=1, linewidth=0.1, edgecolor='gray', width=width)        
   
    
# # plt.xticks(months)
# # plt.xticks(months, labels = months_lbl)
# # plt.xlim(0, 13)
# # plt.xlabel('month')
        
# ax1 = plt.subplot(111)        
# sns.boxplot(gaps_stat, palette=cmap_months, saturation=0.5)
# plt.xticks(months-1, labels = months_lbl)
# plt.xlim(-1, 12)
# plt.xlabel('month')
# plt.ylabel('frequency of water gaps [-]\n(2000-2040)')
# ax2 = ax1.twinx()
# plt.plot(months-1, np.cumsum(np.sum(gaps_stat,0))/np.sum(gaps_stat)*100, 'k--' )
# plt.ylabel('cumulative water gap [%]')

# ax3 = plt.subplot(212)
# # plt.barplot(x=months,height=f, linewidth=0.5, edgecolor='k')
# sns.barplot(x=months, y=sum(gaps_stat), hue=sum(gaps_stat), legend=False, palette=cmap)
# plt.xticks(months-1)
# plt.xticks(months-1, labels = months_lbl)
# plt.xlim(-1, 12)
# plt.ylabel('total water gap\noccurence [-]')
# plt.xlabel('month')
# # ax4 = ax3.twinx()
# plt.bar(months,sum(gaps_stat)/len(sum(gaps_stat)), alpha=0.0)
# plt.ylabel('total water gap\nfrequency [%]')







#%%
months = np.linspace(1,12,12)
months_lbl = ('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dez')
cmap = 'RdYlGn_r' #'RdYlBu_r' #'seismic'
cmap_months = 'twilight'
alpha = 0.9

#%
plot_opt = {"add_colorbar": False, "add_labels":False, "linewidth" :0.003, "color" : 'k'}
cbar_kwargs = {"aspect": 35, "label": "water gap [-]","ticks": np.linspace(0,0.8255,3)}

def fig_4():
    fig = plt.subplots(1,6, num=4, figsize=(17,4), clear=True, gridspec_kw={'width_ratios': [0.8, 0.8, 0.8, 0.8, 0.8, 1]})
    plt.subplots_adjust(left=0.15, right=0.9, bottom=0.15, top=0.85, wspace=0.2, hspace=0.3)
    
    ax1 = plt.subplot(161)
    norm_da.sel(time=slice(norm_da.time[0],norm_da.time[11])).mean('time').plot(cmap=cmap,ax=ax1, alpha=alpha, **plot_opt)
    plt.gca().invert_yaxis()
    plt.yticks(y ,labels = names, rotation=0)
    plt.xticks(months-1, labels = months_lbl, rotation=90)
    plt.title('mean monthly water gap\n(2000-2010)')
    
    ax2 = plt.subplot(162)
    norm_da.sel(time=slice(norm_da.time[12],norm_da.time[21])).mean('time').plot(cmap=cmap,ax=ax2, alpha=alpha, **plot_opt)
    plt.gca().invert_yaxis()
    plt.yticks(y, labels = '')
    plt.ylabel('')
    plt.xticks(months-1, labels = months_lbl, rotation=90)
    plt.title('mean monthly water gap\n(2010-2020)')
    
    ax3 = plt.subplot(163)
    norm_da.sel(time=slice(norm_da.time[22],norm_da.time[31])).mean('time').plot(cmap=cmap,ax=ax3, alpha=alpha, **plot_opt)
    plt.gca().invert_yaxis()
    plt.yticks(y, labels = '')
    plt.ylabel('')
    plt.xticks(months-1, labels = months_lbl, rotation=90)
    plt.title('mean monthly water gap\n(2020-2030)')
    
    ax4 = plt.subplot(164)
    norm_da.sel(time=slice(norm_da.time[32],norm_da.time[41])).mean('time').plot(cmap=cmap,ax=ax4, alpha=alpha, **plot_opt)
    plt.gca().invert_yaxis()
    plt.yticks(y, labels = '')
    plt.ylabel('')
    plt.xticks(months-1, labels = months_lbl, rotation=90)
    plt.title('mean monthly water gap\n(2030-2040)')
    
    ax5 = plt.subplot(165)
    norm_da.sel(time=slice(norm_da.time[42],norm_da.time[51])).mean('time').plot(cmap=cmap,ax=ax5, alpha=alpha, **plot_opt)
    plt.gca().invert_yaxis()
    plt.yticks(y, labels = '')
    plt.ylabel('')
    plt.xticks(months-1, labels = months_lbl, rotation=90)
    plt.title('mean monthly water gap\n(2040-2050)')
    
    ax6 = plt.subplot(166)
    axx6=norm_da.sel(time=slice(norm_da.time[52],norm_da.time[-1])).mean('time').plot(cmap=cmap,ax=ax6, alpha=alpha, **plot_opt)
    plt.gca().invert_yaxis()
    plt.yticks(y, labels = '')
    plt.ylabel('')
    plt.xticks(months-1, labels = months_lbl, rotation=90)
    plt.title('mean monthly water gap\n(2050-2060)')
    
    cbar=plt.colorbar(axx6, **cbar_kwargs)
    cbar.ax.set_yticklabels(['no water gap', 'balanced', 'water gap'])
    return fig

fig = fig_4()
# plt.savefig(f"{figure_path}\\Chennai_decadal_monthly_WG_{scenario}.png", dpi=200)



#%%  maps with mean water gaps per decade
norm = Normalize(0, 1)
def fig_5():
    fig, ax = plt.subplots(2,3, figsize=(19,8), num=5, clear=True)
    plt.subplots_adjust(left=0.05, right=0.95, bottom=0.1, top=0.95, wspace=0.15, hspace=0.25)
    
    ax = plt.subplot(231)
    data_shape.plot(column = 'y_00_10',  edgecolor='k', linewidth=0.25, norm=norm, cmap=cmap, legend=False, ax=ax)
    ctx.add_basemap(ax, zoom=9, crs='EPSG:4326', alpha=0.7, source="CartoDB.Positron") ## add a basemap ("CartoDB.Voyager", "CartoDB.Positron")
    plt.title('mean water gap (2000-2010)')
    
    ax = plt.subplot(232)
    data_shape.plot(column = 'y_10_20',  edgecolor='k', linewidth=0.25, norm=norm, cmap=cmap, legend=False, ax=ax)
    ctx.add_basemap(ax, zoom=9, crs='EPSG:4326', alpha=0.7, source="CartoDB.Positron") ## add a basemap ("CartoDB.Voyager", "CartoDB.Positron")
    plt.title('mean water gap (2010-2020)')
    
    ax = plt.subplot(233)
    data_shape.plot(column = 'y_20_30',  edgecolor='k', linewidth=0.25, norm=norm, cmap=cmap, legend=False, ax=ax)
    ctx.add_basemap(ax, zoom=9, crs='EPSG:4326', alpha=0.7, source="CartoDB.Positron") ## add a basemap ("CartoDB.Voyager", "CartoDB.Positron")
    cbar = plt.cm.ScalarMappable(norm=norm, cmap=cmap)
    ax_cbar = fig.colorbar(cbar, ax=ax)  # add colorbar 
    ax_cbar.set_ticks(np.linspace(0,1,11))
    ax_cbar.set_label('water gap [-]', labelpad=20)   # add label for the colorbar
    plt.title('mean water gap (2020-2030)')
    
    ax = plt.subplot(234)
    data_shape.plot(column = 'y_30_40',  edgecolor='k', linewidth=0.25, norm=norm, cmap=cmap, legend=False, ax=ax)
    ctx.add_basemap(ax, zoom=9, crs='EPSG:4326', alpha=0.7, source="CartoDB.Positron") ## add a basemap ("CartoDB.Voyager", "CartoDB.Positron")
    plt.xlabel(f"Longitude [{get_super(' o')}]", labelpad=10.0)
    plt.title('mean water gap (2030-2040)')
    
    ax = plt.subplot(235)
    data_shape.plot(column = 'y_40_50',  edgecolor='k', linewidth=0.25, norm=norm, cmap=cmap, legend=False, ax=ax)
    ctx.add_basemap(ax, zoom=9, crs='EPSG:4326', alpha=0.7, source="CartoDB.Positron") ## add a basemap ("CartoDB.Voyager", "CartoDB.Positron")
    plt.xlabel(f"Longitude [{get_super(' o')}]", labelpad=10.0)
    plt.title('mean water gap (2040-2050)')
    
    ax = plt.subplot(236)
    data_shape.plot(column = 'y_50_60',  edgecolor='k', linewidth=0.25, norm=norm, cmap=cmap, legend=False, ax=ax)
    ctx.add_basemap(ax, zoom=9, crs='EPSG:4326', alpha=0.7, source="CartoDB.Positron") ## add a basemap ("CartoDB.Voyager", "CartoDB.Positron")
    cbar = plt.cm.ScalarMappable(norm=norm, cmap=cmap)
    ax_cbar = fig.colorbar(cbar, ax=ax)  # add colorbar 
    ax_cbar.set_ticks(np.linspace(0,1,11))
    ax_cbar.set_label('water gap [-]', labelpad=20)   # add label for the colorbar
    plt.title('mean water gap (2050-2060)')
    
    plt.xlabel(f"Longitude [{get_super(' o')}]", labelpad=10.0)
    plt.ylabel(f"Latitude [{get_super(' o')}]", labelpad=10.0)
    return fig
fig = fig_5()
# plt.savefig(f"{figure_path}\\Chennai_decadal mean_WG_map_{scenario}.png", dpi=200)

#%%  maps with number of water gaps per decade
oc_max = max(np.max(data_shape[data_shape.columns[-4:]]))
       
norm = Normalize(0, oc_max)
def fig_6():
    fig, ax = plt.subplots(2,3, figsize=(19,8), num=6, clear=True)
    plt.subplots_adjust(left=0.05, right=0.95, bottom=0.1, top=0.95, wspace=0.15, hspace=0.25)
    
    ax = plt.subplot(231)
    data_shape.plot(column = 'y_00_10_n',  edgecolor='k', linewidth=0.25, norm=norm, cmap=cmap, legend=False, ax=ax)
    ctx.add_basemap(ax, zoom=9, crs='EPSG:4326', alpha=0.7, source="CartoDB.Positron") ## add a basemap ("CartoDB.Voyager", "CartoDB.Positron")
    plt.title('number of months with a water gap (2000-2010)')
    
    ax = plt.subplot(232)
    data_shape.plot(column = 'y_10_20_n',  edgecolor='k', linewidth=0.25, norm=norm, cmap=cmap, legend=False, ax=ax)
    ctx.add_basemap(ax, zoom=9, crs='EPSG:4326', alpha=0.7, source="CartoDB.Positron") ## add a basemap ("CartoDB.Voyager", "CartoDB.Positron")
    plt.title('number of months with a water gap (2010-2020)')
    
    
    ax = plt.subplot(233)
    data_shape.plot(column = 'y_20_30_n',  edgecolor='k', linewidth=0.25, norm=norm, cmap=cmap, legend=False, ax=ax)
    ctx.add_basemap(ax, zoom=9, crs='EPSG:4326', alpha=0.7, source="CartoDB.Positron") ## add a basemap ("CartoDB.Voyager", "CartoDB.Positron")
    plt.title('number of months with a water gap (2020-2030)')
    
    cbar = plt.cm.ScalarMappable(norm=norm, cmap=cmap)
    ax_cbar = fig.colorbar(cbar, ax=ax)  # add colorbar 
    ax_cbar.set_ticks(np.linspace(0,oc_max,5))
    ax_cbar.set_label('number of months [-]', labelpad=20)   # add label for the colorbar
        
    
    ax = plt.subplot(234)
    data_shape.plot(column = 'y_30_40_n',  edgecolor='k', linewidth=0.25, norm=norm, cmap=cmap, legend=False, ax=ax)
    ctx.add_basemap(ax, zoom=9, crs='EPSG:4326', alpha=0.7, source="CartoDB.Positron") ## add a basemap ("CartoDB.Voyager", "CartoDB.Positron")
    plt.xlabel(f"Longitude [{get_super(' o')}]", labelpad=10.0)
    plt.title('number of months with a water gap (2030-2040)')
    
    ax = plt.subplot(235)
    data_shape.plot(column = 'y_40_50_n',  edgecolor='k', linewidth=0.25, norm=norm, cmap=cmap, legend=False, ax=ax)
    ctx.add_basemap(ax, zoom=9, crs='EPSG:4326', alpha=0.7, source="CartoDB.Positron") ## add a basemap ("CartoDB.Voyager", "CartoDB.Positron")
    plt.xlabel(f"Longitude [{get_super(' o')}]", labelpad=10.0)
    plt.title('number of months with a water gap (2040-2050)')
    
    
    ax = plt.subplot(236)
    data_shape.plot(column = 'y_50_60_n',  edgecolor='k', linewidth=0.25, norm=norm, cmap=cmap, legend=False, ax=ax)
    ctx.add_basemap(ax, zoom=9, crs='EPSG:4326', alpha=0.7, source="CartoDB.Positron") ## add a basemap ("CartoDB.Voyager", "CartoDB.Positron")
    
    cbar = plt.cm.ScalarMappable(norm=norm, cmap=cmap)
    ax_cbar = fig.colorbar(cbar, ax=ax)  # add colorbar 
    ax_cbar.set_ticks(np.linspace(0,oc_max,5))
    ax_cbar.set_label('number of months [-]', labelpad=10)   # add label for the colorbar
    plt.title('number of months with a water gap (2050-2060)')
    plt.xlabel(f"Longitude [{get_super(' o')}]", labelpad=10.0)
    plt.ylabel(f"Latitude [{get_super(' o')}]", labelpad=10.0)
    return fig

fig = fig_6()
# plt.savefig(f"{figure_path}\\Chennai_decadal_count_WG_map_{scenario}.png", dpi=200)

#%%

WG_sum = np.sum(gap,0)
year_occ = np.sum(da_sel.sum(dim='location'),1)


norm = Normalize(min(WG_sum),max(WG_sum))     

def fig_7():
    fig = plt.subplots(2,1, figsize=(13,8), num=7, clear=True)
    plt.subplots_adjust(left=0.05, right=0.95, bottom=0.2, top=0.9, wspace=0.15, hspace=0.6)
    
    plt.subplot(211)
    sns.barplot(x=np.arange(0,len(WG_sum)), y=[1]*len(WG_sum), hue=WG_sum, 
                legend=False, palette='seismic', width=1.0, gap=0)
    plt.xticks(np.arange(-0.5,len(WG_sum),12), labels = np.arange(2000,time_year[-1].year+2),
               rotation=45, size=10, color=(0.8, 0.8, 0.8))
    # plt.xticks([])
    plt.yticks([])
    plt.xlim(-0.5,len(WG_sum))
    plt.ylim(0,1)
    plt.title('monthly water gap occurences')
    
    plt.subplot(212)
    # plt.plot(np.arange(0,492), np.cumsum(WG_sum)/np.sum(WG_sum)*100, 'k--', alpha=0.5)
    plt.plot(np.arange(6,len(WG_sum),12), year_occ, 'k.--', linewidth=0.5, alpha=0.8)
    # plt.fill_between(np.arange(6,492,12), [0]*41, year_occ, alpha=0.8, color=(0.8, 0.8, 0.8))
    plt.ylim(year_occ.min()*.95,year_occ.max()*1.05)
    plt.yticks([])
    plt.xticks(np.arange(-0.5,len(WG_sum),12), labels = np.arange(2000,time_year[-1].year+2),
               rotation=45, size=10, color=(0.8, 0.8, 0.8))
    plt.xlim(-0.5,len(WG_sum))
    # plt.ylim(0,100)
    plt.title('annual water gap occurences')
    
        
    return fig
fig = fig_7()
# plt.savefig(f"{figure_path}\\Chennai_WG_stripes_{scenario}.png", dpi=200)








