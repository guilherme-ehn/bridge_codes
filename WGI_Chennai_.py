# -*- coding: utf-8 -*-
"""
Created on Fri Jan  5 17:18:10 2024

@author: emidioho
"""

import glob
import pandas as pd
import numpy as np
import geopandas as gpd
import xarray as xr

import contextily as ctx

import seaborn as sns
from matplotlib import pyplot as plt
from matplotlib.colors import Normalize
plt.style.use('seaborn-v0_8-ticks') #plt.style.available[24], ggplot, seaborn-v0_8-whitegrid, seaborn-v0_8-ticks
# from GN_functions import get_super

# from matplotlib.colors import LinearSegmentedColormap


#%% WGI calculation (from _calculate_WGI - GitLab)


def count_sequences(sequence, treshhold):
    counts = []
    for j in range(np.size(sequence,0)):
        count = 0
        counted = False
        for i in sequence.iloc[j]:
            # print(i)
            if i > treshhold and not counted:
                count = count+1
                counted = True
            if i <= treshhold:
                counted = False
        counts.append(count)    
    return counts


def get_max_of_sequences(sequence, treshhold):
    maxs_all =[]
    for j in range(np.size(sequence,0)):
        # print(j)
        current_sequence = []
        maxs = []
        for i in range(np.size(sequence.iloc[j])):
            # print(sequence[j,i])
            if sequence.iloc[j,i] > treshhold:
                current_sequence.append(sequence.iloc[j,i])
            if sequence.iloc[j,i] <= treshhold and len(current_sequence)>0:
                maxs.append(max(current_sequence))
                current_sequence = []
        if len(current_sequence)>0:
           maxs.append(max(current_sequence))
        try:
            maxs_all.append(max(maxs))
        except:
            maxs_all.append(0)
    return maxs_all



#%% folder paths to save some files
figure_path = r"C:\Users\emidioho\OneDrive - Stichting Deltares\Documents\Projects\Bridge\figures"
folder_path = r"C:\Users\emidioho\OneDrive - Stichting Deltares\Documents\Projects\Bridge"
#### read shapefile (only used for plotting, so it does not matter the SSP case)
data_path = r"N:\Projects\1230000\1230409\B. Measurements and calculations\26.dashboarddata\chennai"
data_shape = gpd.read_file(r'N:\Projects\1230000\1230409\B. Measurements and calculations\26.dashboarddata\chennai\shp\areas.shp')   


## removing area_id = 6 (one feature less on the WG data)
data_shape = data_shape.drop(data_shape.index[7])
data_shape = data_shape.reset_index()
names = data_shape.name

#companies data:
data_comp = pd.read_csv(r'N:\Projects\1230000\1230409\B. Measurements and calculations\17. Site in India - Chennai\Agriculture and Industry\Chennai_AssetLevelData.csv', sep=';')
data_comp = data_comp.dropna()
data_comp = gpd.GeoDataFrame(data_comp, geometry=gpd.points_from_xy(data_comp.x, data_comp.y), crs="EPSG:4326")

#%%##########################################################################
scenario = 'SSP3'   # either SSP2 or SSP3


        
#%% Demand and availability files from N drive
availability = pd.read_csv(f'N:/Projects/1230000/1230409/B. Measurements and calculations/26.dashboarddata/chennai/csv/waterAvailability_Riverandpercipitation_{scenario}_none.csv')
availability = availability[availability.columns[1:]]
# availability = availability[availability.columns[1:np.size(demand,1)+1]]


### open all SSPx files with monthly waterDemand
files_list = glob.glob(f'{data_path}\csv\waterDemand*.csv')
files_list = [x for x in files_list if f'{scenario}' in x]

# sum all demands
for i in range(np.size(files_list)):
    data = pd.read_csv(files_list[0])
    if i ==0:
        demand = data
    else:
        demand = sum([demand, data])


#%%## WGI
gap = demand - availability
# gap.to_csv(f"{folder_path}/water_gap_Chennai_{scenario}.csv")

count_months = np.size(gap,1)
count_months_target_met = np.sum(gap<=0,1)
count_months_target_not_met = count_months - count_months_target_met
count_failures = count_sequences(gap,0)
# sum_max_gap = np.sum(get_max_of_sequences(gap/demand,0))
sum_max_gap = get_max_of_sequences(gap/demand,0)

#print("count_months", count_months)
#print("count_months_target_met", count_months_target_met)
#print("count_months_target_not_met", count_months_target_not_met)
#print("count_failures", count_failures)
#print("sum_max_gap", sum_max_gap)

# reliability aka frequency
# 0 = lowest hazard = no gaps
# 1 = highest hazard = always a gap
reliability = count_months_target_not_met / count_months

# resilience aka persistence
# 0 = lowest hazard = gaps are short and are scattered over the whole time series
# 1 = highest hazard = there is one, very long, gap
resilience = []
for i in range(np.size(count_months_target_not_met)):
    if count_months_target_not_met[i] != 0:
        resilience.append(1 - (count_failures[i] / count_months_target_not_met[i]))
    else:
        resilience.append(0) #0=lowest hazard

# vulnerability aka severity
# 0 = lowest hazard = gaps are small, low% of demand
# 1 = highest hazard = gaps are large, hight% of demand
vulnerability = []
for i in range(np.size(count_failures)):
    if count_failures[i] != 0:
        vulnerability.append(sum_max_gap[i] / count_failures[i])
    else:
        vulnerability.append(0) #0=lowest risk


WGI = (reliability + resilience + vulnerability)/3

#%%# merging data back to shapefile
data_shape = data_shape.assign(reliability = reliability)
data_shape = data_shape.assign(resilience = resilience)
data_shape = data_shape.assign(vulnerability = vulnerability)
data_shape = data_shape.assign(WGI = WGI)

# data_shape.to_file(f'{folder_path}/WGI_Chennai_{scenario}.shp', driver='ESRI Shapefile')


#%% total demand per user
user = 'Agriculture','DomesticRurAbovePov','DomesticRurBelowPov','DomesticUrbAbovePov','DomesticUrbBelowPov', 'Industry'
i=-1
for user_i in user:
    i+=1
    demand_user = pd.read_csv(f'N:/Projects/1230000/1230409/B. Measurements and calculations/26.dashboarddata/chennai/csv/waterDemand_{user_i}_{scenario}_none.csv')
    demand_user.drop('area_id', axis=1, inplace=True)
    if i==0:
        da_user = xr.DataArray(demand_user)
        da_user=da_user.expand_dims(user = [user_i])
    else:
        demand_user = xr.DataArray(demand_user)
        demand_user = demand_user.expand_dims(user = [user_i])
        
        da_user= xr.concat([da_user, demand_user],dim='user')


da_tot = np.nansum(da_user.sum(dim='user'),0)
x=np.array(da_user.dim_1.values)
y1= np.array( np.nansum(np.nansum(da_user.where(da_user.user=='Agriculture'),1),0))
y2=  np.array( np.nansum(np.nansum(da_user.where(da_user.user=='DomesticRurAbovePov'),1),0))
y3=  np.array( np.nansum(np.nansum(da_user.where(da_user.user=='DomesticRurBelowPov'),1),0))
y4=  np.array( np.nansum(np.nansum(da_user.where(da_user.user=='DomesticUrbAbovePov'),1),0))
y5= np.array( np.nansum(np.nansum(da_user.where(da_user.user=='DomesticUrbBelowPov'),1),0))
y6= np.array( np.nansum(np.nansum(da_user.where(da_user.user=='Industry'),1),0))



####################################################################################
####################################################################################
#%% FIGURES and PLOTS

### figure elements
months = np.linspace(1,12,12)
cmap = 'RdYlGn_r'
cbar_kwargs = {"aspect": 35, "label": "water gap [-]","ticks": np.linspace(0,0.8255,3)}
alpha = 0.9

#%% fig tot_demand over time
fig,ax = plt.subplots(2,1, figsize=(12,8), num=220, clear=True)    
plt.subplots_adjust(left=0.10, right=0.90, bottom=0.1, top=0.95, wspace=0.05, hspace=0.35)

ax1 = plt.subplot(211)
plt.stackplot(x, (y1/da_tot)*100, (y2/da_tot)*100, (y3/da_tot)*100, (y4/da_tot)*100, (y5/da_tot)*100, (y6/da_tot)*100, linewidth=0.25, edgecolor='k')
plt.xticks(np.arange(0,718,718/50+1).astype(int), (np.arange(0,718,718/50+1)/12).astype(int)+2000, rotation=45, color=(0.5, 0.5, 0.5))
plt.xlim(0,718)
plt.ylabel('percentage of demand')
plt.legend(['Agriculture','DomesticRurAbovePov','DomesticRurBelowPov','DomesticUrbAbovePov','DomesticUrbBelowPov', 'Industry'], frameon=True, edgecolor='k', ncols=3, loc=9) 
 
ax2 = plt.subplot(212)
plt.plot(x, y1)
plt.plot(x, y2)
plt.plot(x, y3)
plt.plot(x, y4)
plt.plot(x, y5)
plt.plot(x, y6)
plt.plot(x,np.nansum(availability,0),'k--')
plt.xticks(np.arange(0,718,718/50+1).astype(int), (np.arange(0,718,718/50+1)/12).astype(int)+2000, rotation=45, color=(0.5, 0.5, 0.5))
plt.xlim(0,718)
# plt.ylim(0,10000)
plt.legend(['Agriculture','DomesticRurAbovePov','DomesticRurBelowPov','DomesticUrbAbovePov','DomesticUrbBelowPov', 'Industry'], frameon=True, edgecolor='k', ncols=3, loc=9)  

# plt.savefig(f"{figure_path}\\tot_demand_vs_avail_CH_{scenario}.png", dpi=300)



#%% map with names and companies count

#merging polygons and industries points
dfsjoin = gpd.sjoin(data_shape, data_comp) #Spatial join Points to polygons
dfpivot = pd.pivot_table(dfsjoin,index='area_id',columns='indstry',aggfunc={'indstry':len})
dfpivot.columns = dfpivot.columns.droplevel()
data_shape_ind = data_shape.merge(dfpivot, how='left', on='area_id')

# data_shape_ind.iloc[:,5:] = data_shape_ind.iloc[:,5:].where(~np.isnan(data_shape_ind.iloc[:,5:].values),0)

data_shape_n = data_shape
data_shape_n['coords'] = data_shape['geometry'].apply(lambda x: x.representative_point().coords[:])
data_shape_n['coords'] = [coords[0] for coords in data_shape_n['coords']]
data_shape_n['tot_ind'] = np.nansum(data_shape_ind.iloc[:,5:],1)

fig, ax = plt.subplots(figsize=(8,8), num=450, clear=True)
plt.subplots_adjust(left=0.15, right=0.9, bottom=0.15, top=0.95, wspace=0.2, hspace=0.3)

data_shape_n.plot(column = data_shape_n.tot_ind, edgecolor='k', linewidth=0.25, legend=False, ax=ax, cmap=cmap)
ctx.add_basemap( ax=ax, zoom=9, crs='EPSG:4326', alpha=0.7, source="CartoDB.Positron") ## add a basemap ("CartoDB.Voyager", "CartoDB.Positron")
for idx, row in data_shape_n.iterrows():
    plt.annotate(f"{row['name']}\n {int(row['tot_ind'])}", xy=row['coords'], horizontalalignment='center',size=8, color='w')
    
del data_shape_n

plt.savefig(f"{figure_path}\\CH_map_industries_count.png", dpi=300)

#%% RRV maps
norm = Normalize(0, 1)
fig, ax = plt.subplots(3,1, figsize=(7,8), num=11, clear=True)
plt.subplots_adjust(left=0.05, right=0.80, bottom=0.1, top=0.95, wspace=0.25, hspace=0.15)

ax = plt.subplot(311)
data_shape.plot(column = 'reliability',  edgecolor='k', linewidth=0.25, norm=norm, cmap=cmap, legend=False, ax=ax)
ctx.add_basemap(ax, zoom=9, crs='EPSG:4326', alpha=0.7, source="CartoDB.Positron") ## add a basemap ("CartoDB.Voyager", "CartoDB.Positron")
cbar = plt.cm.ScalarMappable(norm=norm, cmap=cmap)
ax_cbar = fig.colorbar(cbar, ax=ax, shrink =0.95)  # add colorbar 
ax_cbar.set_ticks(np.linspace(0,1,2))
ax_cbar.set_ticklabels(['few gaps', 'many gaps'])
plt.ylabel('reliability\n(frequency)', labelpad=20)   # add label for the colorbar
plt.xticks([],'')
# plt.title('reliability')

ax = plt.subplot(312)
data_shape.plot(column = 'resilience',  edgecolor='k', linewidth=0.25, norm=norm, cmap=cmap, legend=False, ax=ax)
ctx.add_basemap(ax, zoom=9, crs='EPSG:4326', alpha=0.7, source="CartoDB.Positron") ## add a basemap ("CartoDB.Voyager", "CartoDB.Positron")
cbar = plt.cm.ScalarMappable(norm=norm, cmap=cmap)
ax_cbar = fig.colorbar(cbar, ax=ax, shrink =0.95)  # add colorbar 
ax_cbar.set_ticks(np.linspace(0,1,2))
ax_cbar.set_ticklabels(['short gaps', 'long gaps'])
plt.ylabel('resilience\n(persistence)', labelpad=20)   # add label for the colorbar
plt.xticks([],'')
# plt.title('resilience')

ax = plt.subplot(313)
data_shape.plot(column = 'vulnerability',  edgecolor='k', linewidth=0.25, norm=norm, cmap=cmap, legend=False, ax=ax)
ctx.add_basemap(ax, zoom=9, crs='EPSG:4326', alpha=0.7, source="CartoDB.Positron") ## add a basemap ("CartoDB.Voyager", "CartoDB.Positron")
cbar = plt.cm.ScalarMappable(norm=norm, cmap=cmap)
ax_cbar = fig.colorbar(cbar, ax=ax, shrink =0.95)  # add colorbar 
ax_cbar.set_ticks(np.linspace(0,1,2))
ax_cbar.set_ticklabels(['small gaps', 'large gaps'])
plt.ylabel('vulnerability\n(severity)', labelpad=20)   # add label for the colorbar
# plt.title('vulnerability')

# plt.savefig(f"{figure_path}\\RRV_Chennai_{scenario}.png", dpi=300)


#%% map WGI
fig, ax = plt.subplots(figsize=(10,8), num=161, clear=True)
plt.subplots_adjust(left=0.15, right=0.95, bottom=0.1, top=0.95, wspace=0.25, hspace=0.15)

norm = Normalize(0, 1)
ax = data_shape.plot(column = 'WGI',  edgecolor='k', linewidth=0.25, norm=norm, cmap=cmap, legend=False, ax=ax)
ctx.add_basemap(ax, zoom=9, crs='EPSG:4326', alpha=0.7, source="CartoDB.Positron") ## add a basemap ("CartoDB.Voyager", "CartoDB.Positron")
cbar = plt.cm.ScalarMappable(norm=norm, cmap=cmap)
ax_cbar = fig.colorbar(cbar, ax=ax, shrink =0.5)  # add colorbar 
ax_cbar.set_ticks(np.linspace(0,1,11))
ax_cbar.set_label('WGI [-]', labelpad=20)   # add label for the colorbar
# plt.xticks([],'')

# plt.savefig(f"{figure_path}\\WGI_CH_{scenario}.png", dpi=300)



#%% map WGI  and INDUSTRIES
fig, ax = plt.subplots(figsize=(16,8), num=161, clear=True)
plt.subplots_adjust(left=0.15, right=0.95, bottom=0.1, top=0.95, wspace=0.25, hspace=0.15)

norm = Normalize(0, 1)
ax = data_shape.plot(column = 'WGI',  edgecolor='k', linewidth=0.25, norm=norm, cmap=cmap, legend=False, ax=ax, alpha=0.9)
ctx.add_basemap(ax, zoom=9, crs='EPSG:4326', alpha=0.7, source="CartoDB.Positron") ## add a basemap ("CartoDB.Voyager", "CartoDB.Positron")
cbar = plt.cm.ScalarMappable(norm=norm, cmap=cmap)
ax_cbar = fig.colorbar(cbar, ax=ax, shrink =0.75, alpha=0.9)  # add colorbar 
ax_cbar.set_ticks(np.linspace(0,1,3))
ax_cbar.set_ticklabels(['low hazard', 'med. hazard', 'high hazard'])
ax_cbar.set_label('WGi [-]', labelpad=20)   # add label for the colorbar
# plt.xticks([],'')


# add industries'locations
data_comp = data_comp.dropna()
sns.scatterplot(x=data_comp['longitd'], y=data_comp['latitud'], hue= data_comp['indstry'], edgecolor='k', palette = 'Set1', style= data_comp['indstry'], legend='full')
plt.legend(title='industry type', frameon=True, edgecolor='k',loc=2, fontsize=8)

# plt.savefig(f"{figure_path}\\WGI_CH_and_Indust_{scenario}.png", dpi=300)


#%% histogram WGI

CH_SSP2 = gpd.read_file(f'{folder_path}/WGI_Chennai_SSP2.shp')
WGI_CH_SSP2 = CH_SSP2.WGI/max(CH_SSP2.WGI)
CH_SSP3 = gpd.read_file(f'{folder_path}/WGI_Chennai_SSP3.shp')
WGI_CH_SSP3 = CH_SSP3.WGI/max(CH_SSP3.WGI)

fig, ax = plt.subplots(1,2, figsize=(9,3), num=162, clear=True)
plt.subplots_adjust(left=0.15, right=0.90, bottom=0.1, top=0.90, wspace=0.45, hspace=0.15)


ax1 = plt.subplot(121)
sns.boxplot(data = WGI_CH_SSP2)
plt.title('WGI SSP2')

ax2 = plt.subplot(122)
sns.boxplot(data = WGI_CH_SSP3, color = 'orange')
plt.title('WGI SSP3')

# ax3 = plt.subplot(133)
# plt.plot([0,1.1],[0,1.1],'--k', linewidth=0.5)
# plt.scatter(WGI_CH_SSP2, WGI_CH_SSP3, s=9)
# plt.xlabel('WGI SSP1')
# plt.ylabel('WGI SSP3')
# for i, txt in enumerate(SP_SSP1.NAME_2):
#     ax3.annotate(txt, (WGI_CH_SSP2[i], WGI_CH_SSP3[i]), fontsize=6)

# plt.savefig(f"{figure_path}\\WGI_CH_comparison_boxplot.png", dpi=300)


#%% scatter plots WGI for different SSP

fig, ax = plt.subplots(1,2, figsize=(8,4), num=163, clear=True)
plt.subplots_adjust(left=0.15, right=0.95, bottom=0.15, top=0.90, wspace=0.35, hspace=0.15)

ax2 = plt.subplot(121)
plt.plot([0,1.1],[0,1.1],'--k', linewidth=0.5)
plt.scatter(WGI_CH_SSP2, WGI_CH_SSP3, s=9)
plt.xlabel('WGI SSP2')
plt.ylabel('WGI SSP3')

for i, txt in enumerate(CH_SSP2.name):
    ax2.annotate(txt, (WGI_CH_SSP2[i], WGI_CH_SSP3[i]), fontsize=5)


ax2 = plt.subplot(122)
plt.plot([0,1.1],[0,1.1],'--k', linewidth=0.5)
plt.scatter(WGI_CH_SSP2, WGI_CH_SSP3, s=9)
# plt.xlabel('WGI SSP1')
# plt.ylabel('WGI SSP3')
plt.xlim(0.85, 0.95)
plt.ylim(0.85, 0.95)
for i, txt in enumerate(CH_SSP2.name):
    ax2.annotate(txt, (WGI_CH_SSP2[i], WGI_CH_SSP3[i]), fontsize=8)
    
# plt.savefig(f"{figure_path}\\WGI_CH_comparison_scatter.png", dpi=300)
    


#%% industries count plot
x_loc = np.arange(0,np.size(dfpivot,0))
names_loc = np.linspace(0+0.5,len(data_shape.name)-0.5,len(data_shape.name))

fig, ax = plt.subplots(figsize=(10,5), num=330, clear=True)
plt.subplots_adjust(left=0.65, right=0.9, bottom=0.35, top=0.95, wspace=0.2, hspace=0.3)

sns.heatmap(data_shape_ind.iloc[:,5:].transpose(), cmap=cmap, cbar=False, linewidths=0.003, linecolor='k', alpha=alpha, annot=True, fmt=".0f")
plt.yticks(x_loc+0.5, labels = list(dfpivot.columns), rotation=0)
plt.xticks(names_loc ,labels = data_shape.name, rotation=90)

# plt.savefig(f"{figure_path}\\CH_heat_map_industries_count.png", dpi=300)


# plt.close('all')
