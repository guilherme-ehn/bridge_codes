# -*- coding: utf-8 -*-
"""
Created on Mon Dec 18 11:50:01 2023

@author: emidioho
"""

import pandas as pd
import numpy as np
import geopandas as gpd
import xarray as xr

import contextily as ctx

import seaborn as sns
from matplotlib import pyplot as plt
from matplotlib.colors import Normalize
plt.style.use('seaborn-v0_8-ticks') #plt.style.available[24], ggplot, seaborn-v0_8-whitegrid, seaborn-v0_8-ticks
from GN_functions import get_super

# from matplotlib.colors import LinearSegmentedColormap

#%%
figure_path = r"C:\Users\emidioho\OneDrive - Stichting Deltares\Documents\Projects\Bridge\figures"
#### read shapefiles 
data_path = r"C:\Users\emidioho\OneDrive - Stichting Deltares\Documents\Projects\Bridge"
data_shape = gpd.read_file(f'{data_path}/Bridge_sharepoint/shp/SP/areas_risks_SSP3.shp')   

data_shape['coords'] = data_shape['geometry'].apply(lambda x: x.representative_point().coords[:])
data_shape['coords'] = [coords[0] for coords in data_shape['coords']]

names = data_shape.NAME_2


#### read gapNormalized_SSP3.csv
WG = pd.read_csv(r'N:\Projects\1230000\1230409\B. Measurements and calculations\Risk Calculations\water\gapNormalized_SSP3.csv')
# gN = pd.read_csv(r'N:\Projects\1230000\1230409\B. Measurements and calculations\Risk Calculations\water\gap_SSP3.csv')
WG_data = WG.drop(columns=['id'])

water_gap_dict = { 'Municip': [],
                 'WG_occurence': [],
                 'WG_max_month': [],
                 'WG_tot_occ': [],
                 'WG_data_a': [],
                 }


gaps_stat = []
### compute the number of water_gap ocurrences
for i in range(np.size(WG,0)):
    
    data = np.array(WG.iloc[i][1:])
    data_a = data.reshape(int(np.size(data)/12),12)
    gaps_count = sum(np.where(data_a>0,1,0))
    
    water_gap_dict['Municip'].extend([int(WG.iloc[i][0])] )
    water_gap_dict['WG_occurence'].extend( [gaps_count] )
    water_gap_dict['WG_max_month'].extend( [np.argmax(gaps_count)+1] )
    water_gap_dict['WG_tot_occ'].extend( [sum(gaps_count)] )
    water_gap_dict['WG_data_a'].extend( [data_a] )
    
    if i==0:
        gaps_stat = gaps_count
    else:
        gaps_stat = np.vstack((gaps_stat, gaps_count))

### merging WG back to shapefile
data_shape = data_shape.assign(month_risk = water_gap_dict['WG_max_month'])
data_shape = data_shape.assign(tot_WG = water_gap_dict['WG_tot_occ'])


####################################################################################################################################
#######################################################  data array analysis  ######################################################
####################################################################################################################################
#%% DataArray preparation
a = np.empty((40,12))
a[:] = np.nan
x = np.linspace(0,11,12)
y = np.linspace(0,39,40)

time_year = pd.date_range('1/1/2000', periods=41, freq='Y')
# # start of each year: 
y_start = np.linspace(0,492,42)


for i in range(np.size(time_year,0)):
    data_year = np.array( WG_data[WG_data.columns[int(y_start[i]):int(y_start[i+1])] ] ) 
    
    if i==0:
        da = xr.DataArray(data_year)
        da=da.expand_dims(time = [time_year[i]])
    else:
        data_year = xr.DataArray(data_year)
        data_year = data_year.expand_dims(time = [time_year[i]])
        
        da= xr.concat([da, data_year],dim='time')


da = da.rename({'dim_0' : 'location','dim_1':'month'})
da = da.assign_attrs({'location_name':data_shape.NAME_2 })



da_sel = da.where(da.values==0,np.nan)
da_sel = da_sel.where(~np.isnan(da_sel.values),1)

y_00_10_n = np.array(da_sel.sel(time=slice(da.time[0],da.time[9])).sum('time')) 
y_10_20_n = np.array(da_sel.sel(time=slice(da.time[0],da.time[9])).sum('time')) 
y_30_40_n = np.array(da_sel.sel(time=slice(da.time[30],da.time[-1])).sum('time'))

### merging years_meanWG to shapefile
data_shape = data_shape.assign(y_00_10 = np.array(np.mean(da.sel(time=slice(da.time[0],da.time[9])).mean('time'),1)) )
data_shape = data_shape.assign(y_10_20 = np.array(np.mean(da.sel(time=slice(da.time[10],da.time[19])).mean('time'),1)) )
data_shape = data_shape.assign(y_20_30 = np.array(np.mean(da.sel(time=slice(da.time[20],da.time[29])).mean('time'),1)) )
data_shape = data_shape.assign(y_30_40 = np.array(np.mean(da.sel(time=slice(da.time[30],da.time[-1])).mean('time'),1)) )

### merging counts of years_WG to shapefile
data_shape = data_shape.assign(y_00_10_n = np.array(np.nansum(da_sel.sel(time=slice(da_sel.time[0],da_sel.time[9])).sum('time'),1)) )
data_shape = data_shape.assign(y_10_20_n = np.array(np.nansum(da_sel.sel(time=slice(da_sel.time[10],da_sel.time[19])).sum('time'),1)) )
data_shape = data_shape.assign(y_20_30_n = np.array(np.nansum(da_sel.sel(time=slice(da_sel.time[20],da_sel.time[29])).sum('time'),1)) )
data_shape = data_shape.assign(y_30_40_n = np.sum(y_30_40_n,1))





#%% figure elements
months = np.linspace(1,12,12)
months_lbl = ('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dez')
cmap = 'RdYlGn_r'
cmap_months = 'twilight'
names_loc = np.linspace(0+0.5,len(gaps_stat)-0.5,len(gaps_stat))
alpha = 0.9

annot_kws = {'color':'k', 'size':9}

#% total monthly water gap FIGURE
def fig_2():
    fig, ax = plt.subplots(figsize=(9, 9), num=2, clear=True)
    plt.subplots_adjust(left=0.25, right=0.9, bottom=0.05, top=0.95, wspace=0.2, hspace=0.3)
    
    
    sns.heatmap(gaps_stat, cmap=cmap, cbar=False, linewidths=0.003, linecolor='k', alpha=alpha, annot=True, annot_kws=annot_kws)
    
    cbar = ax.figure.colorbar(ax.collections[0], ticks=np.linspace(0,np.max(gaps_stat),7).astype(int),
                              shrink = 1, aspect= 30, pad = 0.1, alpha=alpha)
    cbar.set_label('total monthly water gaps \n(between 2000 and 2040)', labelpad=10)
    
    plt.xticks(months)
    plt.xticks(months-0.5, labels = months_lbl)
    plt.yticks(names_loc ,labels = names, rotation=0)
    # plt.xlabel('month')
    plt.title('frequency a month presents water gap (between 2000-2040)')
    return fig
fig = fig_2()
# plt.savefig(f"{figure_path}\\frequency_month_WG.png", dpi=200)


#%% MAP
norm = Normalize(1, 12)
def fig_3():
    fig, ax = plt.subplots(figsize=(12,5), num=3, clear=True)
    
    data_shape.plot(column = 'month_risk',  edgecolor='k', linewidth=0.25, norm=norm, cmap=cmap_months, legend=False, ax=ax)
    for idx, row in data_shape.iterrows():
        plt.annotate(text=row['tot_WG'], xy=row['coords'], horizontalalignment='center',size=7)
        
    ctx.add_basemap(ax, zoom=9, crs='EPSG:4326', alpha=0.7, source="CartoDB.Positron") ## add a basemap ("CartoDB.Voyager", "CartoDB.Positron")
    
    cbar = plt.cm.ScalarMappable(norm=norm, cmap=cmap_months)
    ax_cbar = fig.colorbar(cbar, ax=ax)  # add colorbar 
    ax_cbar.set_ticks([1,2,3,4,5,6,7,8,9,10,11,12], labels = months_lbl)
    ax_cbar.set_label('month with more water gaps\n (2000-2040)', labelpad=10)   # add label for the colorbar
    
    plt.xlabel(f"Longitude [{get_super(' o')}]", labelpad=10.0)
    plt.ylabel(f"Latitude [{get_super(' o')}]", labelpad=10.0)
    return fig

fig = fig_3()
# plt.savefig(f"{figure_path}\\month_more_WG.png", dpi=200)


#%%  Fig 1
width = 0.1
fig, ax = plt.subplots(1,1,num=1, clear=True)
plt.subplots_adjust(left=0.1, right=0.9, bottom=0.15, top=0.9, wspace=0.2, hspace=0.3)

# ax1 = plt.subplot(211)
# for i in range(np.size(water_gap_dict['WG_data_a'],0)):
#     if i in np.int16(np.linspace(1,39,20)):
#         for ii in range(np.size(water_gap_dict['WG_data_a'][i],0)):
#             if ii<11:        
#                 plt.bar(months,water_gap_dict['WG_data_a'][i][ii], alpha=1, linewidth=0.1, edgecolor='g',width=width)
#             elif ii>10 and ii<21:        
#                 plt.bar(months +ii*0.02,water_gap_dict['WG_data_a'][i][ii], alpha=1, linewidth=0.1, edgecolor='y', width=width)
#             elif ii>20 and ii<31:        
#                 plt.bar(months +ii*0.02,water_gap_dict['WG_data_a'][i][ii], alpha=1, linewidth=0.1, edgecolor='orange', width=width)  
#             elif ii>30:        
#                 plt.bar(months +ii*0.02,water_gap_dict['WG_data_a'][i][ii], alpha=1, linewidth=0.1, edgecolor='gray', width=width)        
   
    
# plt.xticks(months)
# plt.xticks(months, labels = months_lbl)
# plt.xlim(0, 13)
# plt.xlabel('month')
        
ax1 = plt.subplot(111)        
sns.boxplot(gaps_stat, palette=cmap_months, saturation=0.5)
plt.xticks(months-1, labels = months_lbl)
plt.xlim(-1, 12)
plt.xlabel('month')
plt.ylabel('frequency of water gaps [-]\n(2000-2040)')
ax2 = ax1.twinx()
plt.plot(months-1, np.cumsum(np.sum(gaps_stat,0))/np.sum(gaps_stat)*100, 'k--' )
plt.ylabel('cumulative water gap [%]')

# ax3 = plt.subplot(212)
# # plt.barplot(x=months,height=f, linewidth=0.5, edgecolor='k')
# sns.barplot(x=months, y=sum(gaps_stat), hue=sum(gaps_stat), legend=False, palette=cmap)
# plt.xticks(months-1)
# plt.xticks(months-1, labels = months_lbl)
# plt.xlim(-1, 12)
# plt.ylabel('total water gap\noccurence [-]')
# plt.xlabel('month')
# # ax4 = ax3.twinx()
# plt.bar(months,sum(gaps_stat)/len(sum(gaps_stat)), alpha=0.0)
# plt.ylabel('total water gap\nfrequency [%]')







#%%
months = np.linspace(1,12,12)
months_lbl = ('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dez')
cmap = 'RdYlGn_r' #'RdYlBu_r' #'seismic'
cmap_months = 'twilight'
alpha = 0.9

#%
plot_opt = {"add_colorbar": False, "add_labels":False, "linewidth" :0.003, "color" : 'k'}
cbar_kwargs = {"aspect": 35, "label": "water gap [-]","ticks": np.linspace(0,1,3)}

def fig_4():
    fig = plt.subplots(1,4, num=4, figsize=(15,9), clear=True, gridspec_kw={'width_ratios': [0.8, 0.8, 0.8, 1]})
    plt.subplots_adjust(left=0.15, right=0.9, bottom=0.1, top=0.90, wspace=0.2, hspace=0.3)
    
    ax1 = plt.subplot(141)
    da.sel(time=slice(da.time[0],da.time[9])).mean('time').plot(cmap=cmap,ax=ax1, alpha=alpha, **plot_opt)
    plt.gca().invert_yaxis()
    plt.yticks(y ,labels = data_shape.NAME_2, rotation=0)
    plt.xticks(months-1, labels = months_lbl, rotation=90)
    plt.title('mean monthly water gap\n(2000-2010)')
    
    ax2 = plt.subplot(142)
    da.sel(time=slice(da.time[10],da.time[19])).mean('time').plot(cmap=cmap,ax=ax2, alpha=alpha, **plot_opt)
    plt.gca().invert_yaxis()
    plt.yticks(y, labels = '')
    plt.ylabel('')
    plt.xticks(months-1, labels = months_lbl, rotation=90)
    plt.title('mean monthly water gap\n(2010-2020)')
    
    ax3 = plt.subplot(143)
    da.sel(time=slice(da.time[20],da.time[29])).mean('time').plot(cmap=cmap,ax=ax3, alpha=alpha, **plot_opt)
    plt.gca().invert_yaxis()
    plt.yticks(y, labels = '')
    plt.ylabel('')
    plt.xticks(months-1, labels = months_lbl, rotation=90)
    plt.title('mean monthly water gap\n(2020-2030)')
    
    ax4 = plt.subplot(144)
    axx4=da.sel(time=slice(da.time[30],da.time[-1])).mean('time').plot(cmap=cmap,ax=ax4, alpha=alpha, **plot_opt)
    plt.gca().invert_yaxis()
    plt.yticks(y, labels = '')
    plt.ylabel('')
    plt.xticks(months-1, labels = months_lbl, rotation=90)
    plt.title('mean monthly water gap\n(2030-2040)')
    
    cbar=plt.colorbar(axx4, **cbar_kwargs)
    cbar.ax.set_yticklabels(['no water gap', 'balanced', 'water gap'])
    return fig

fig = fig_4()
# plt.savefig(f"{figure_path}\\decadal_monthly_WG.png", dpi=200)



#%%  maps with mean water gaps per decade
norm = Normalize(0, 1)
def fig_5():
    fig, ax = plt.subplots(2,2, figsize=(16,8), num=5, clear=True)
    plt.subplots_adjust(left=0.05, right=0.95, bottom=0.1, top=0.95, wspace=0.15, hspace=0.25)
    
    ax = plt.subplot(221)
    data_shape.plot(column = 'y_00_10',  edgecolor='k', linewidth=0.25, norm=norm, cmap=cmap, legend=False, ax=ax)
    ctx.add_basemap(ax, zoom=9, crs='EPSG:4326', alpha=0.7, source="CartoDB.Positron") ## add a basemap ("CartoDB.Voyager", "CartoDB.Positron")
    plt.title('mean water gap (2000-2010)')
    
    ax = plt.subplot(222)
    data_shape.plot(column = 'y_10_20',  edgecolor='k', linewidth=0.25, norm=norm, cmap=cmap, legend=False, ax=ax)
    ctx.add_basemap(ax, zoom=9, crs='EPSG:4326', alpha=0.7, source="CartoDB.Positron") ## add a basemap ("CartoDB.Voyager", "CartoDB.Positron")
    cbar = plt.cm.ScalarMappable(norm=norm, cmap=cmap)
    ax_cbar = fig.colorbar(cbar, ax=ax)  # add colorbar 
    ax_cbar.set_ticks(np.linspace(0,1,11))
    ax_cbar.set_label('water gap [-]', labelpad=20)   # add label for the colorbar
    plt.title('mean water gap (2010-2020)')
    
    ax = plt.subplot(223)
    data_shape.plot(column = 'y_20_30',  edgecolor='k', linewidth=0.25, norm=norm, cmap=cmap, legend=False, ax=ax)
    ctx.add_basemap(ax, zoom=9, crs='EPSG:4326', alpha=0.7, source="CartoDB.Positron") ## add a basemap ("CartoDB.Voyager", "CartoDB.Positron")
    plt.xlabel(f"Longitude [{get_super(' o')}]", labelpad=10.0)
    plt.title('mean water gap (2020-2030)')
    
    ax = plt.subplot(224)
    data_shape.plot(column = 'y_30_40',  edgecolor='k', linewidth=0.25, norm=norm, cmap=cmap, legend=False, ax=ax)
    ctx.add_basemap(ax, zoom=9, crs='EPSG:4326', alpha=0.7, source="CartoDB.Positron") ## add a basemap ("CartoDB.Voyager", "CartoDB.Positron")
    cbar = plt.cm.ScalarMappable(norm=norm, cmap=cmap)
    ax_cbar = fig.colorbar(cbar, ax=ax)  # add colorbar 
    ax_cbar.set_ticks(np.linspace(0,1,11))
    ax_cbar.set_label('water gap [-]', labelpad=20)   # add label for the colorbar
    plt.title('mean water gap (2030-2040)')
    
    plt.xlabel(f"Longitude [{get_super(' o')}]", labelpad=10.0)
    plt.ylabel(f"Latitude [{get_super(' o')}]", labelpad=10.0)
    return fig
fig = fig_5()
# plt.savefig(f"{figure_path}\\decadal mean_WG_map.png", dpi=200)

#%%  maps with number of water gaps per decade
oc_max = max(np.max(data_shape[data_shape.columns[-4:]]))
       
norm = Normalize(0, oc_max)
def fig_6():
    fig, ax = plt.subplots(2,2, figsize=(16,8), num=6, clear=True)
    plt.subplots_adjust(left=0.05, right=0.95, bottom=0.1, top=0.95, wspace=0.15, hspace=0.25)
    
    ax = plt.subplot(221)
    data_shape.plot(column = 'y_00_10_n',  edgecolor='k', linewidth=0.25, norm=norm, cmap=cmap, legend=False, ax=ax)
    ctx.add_basemap(ax, zoom=9, crs='EPSG:4326', alpha=0.7, source="CartoDB.Positron") ## add a basemap ("CartoDB.Voyager", "CartoDB.Positron")
    plt.title('number of months with a water gap (2000-2010)')
    
    
    ax = plt.subplot(222)
    data_shape.plot(column = 'y_10_20_n',  edgecolor='k', linewidth=0.25, norm=norm, cmap=cmap, legend=False, ax=ax)
    ctx.add_basemap(ax, zoom=9, crs='EPSG:4326', alpha=0.7, source="CartoDB.Positron") ## add a basemap ("CartoDB.Voyager", "CartoDB.Positron")
    plt.title('number of months with a water gap (2010-2020)')
    
    cbar = plt.cm.ScalarMappable(norm=norm, cmap=cmap)
    ax_cbar = fig.colorbar(cbar, ax=ax)  # add colorbar 
    ax_cbar.set_ticks(np.linspace(0,oc_max,5))
    ax_cbar.set_label('number of months [-]', labelpad=20)   # add label for the colorbar
        
    
    ax = plt.subplot(223)
    data_shape.plot(column = 'y_20_30_n',  edgecolor='k', linewidth=0.25, norm=norm, cmap=cmap, legend=False, ax=ax)
    ctx.add_basemap(ax, zoom=9, crs='EPSG:4326', alpha=0.7, source="CartoDB.Positron") ## add a basemap ("CartoDB.Voyager", "CartoDB.Positron")
    plt.xlabel(f"Longitude [{get_super(' o')}]", labelpad=10.0)
    plt.title('number of months with a water gap (2020-2030)')
    
    
    ax = plt.subplot(224)
    data_shape.plot(column = 'y_30_40_n',  edgecolor='k', linewidth=0.25, norm=norm, cmap=cmap, legend=False, ax=ax)
    ctx.add_basemap(ax, zoom=9, crs='EPSG:4326', alpha=0.7, source="CartoDB.Positron") ## add a basemap ("CartoDB.Voyager", "CartoDB.Positron")
    
    cbar = plt.cm.ScalarMappable(norm=norm, cmap=cmap)
    ax_cbar = fig.colorbar(cbar, ax=ax)  # add colorbar 
    ax_cbar.set_ticks(np.linspace(0,oc_max,5))
    ax_cbar.set_label('number of months [-]', labelpad=10)   # add label for the colorbar
    
    plt.title('number of months with a water gap (2030-2040)')
    plt.xlabel(f"Longitude [{get_super(' o')}]", labelpad=10.0)
    plt.ylabel(f"Latitude [{get_super(' o')}]", labelpad=10.0)
    return fig

fig = fig_6()
# plt.savefig(f"{figure_path}\\decadal_count_WG_map.png", dpi=200)

#%%

WG_sum = np.sum(WG_data,0)
year_occ = np.sum(da_sel.sum(dim='location'),1)


norm = Normalize(min(WG_sum),max(WG_sum))     

def fig_7():
    fig = plt.subplots(2,1, figsize=(13,8), num=7, clear=True)
    plt.subplots_adjust(left=0.05, right=0.95, bottom=0.2, top=0.9, wspace=0.15, hspace=0.6)
    
    plt.subplot(211)
    sns.barplot(x=np.arange(0,492), y=[1]*492, hue=WG_sum, 
                legend=False, palette='coolwarm', width=1.0, gap=0)
    plt.xticks(np.arange(-0.5,493,12), labels = np.arange(2000,2042),
               rotation=45, size=10, color=(0.8, 0.8, 0.8))
    # plt.xticks([])
    plt.yticks([])
    plt.xlim(-0.5,492)
    plt.ylim(0,1)
    plt.title('monthly water gap occurences')
    
    plt.subplot(212)
    # plt.plot(np.arange(0,492), np.cumsum(WG_sum)/np.sum(WG_sum)*100, 'k--', alpha=0.5)
    plt.plot(np.arange(6,492,12), year_occ, 'k.--', linewidth=0.5, alpha=0.8)
    # plt.fill_between(np.arange(6,492,12), [0]*41, year_occ, alpha=0.8, color=(0.8, 0.8, 0.8))
    plt.ylim(year_occ.min()*.95,year_occ.max()*1.05)
    plt.yticks([])
    plt.xticks(np.arange(-0.5,493,12), labels = np.arange(2000,2042),
               rotation=45, size=10, color=(0.8, 0.8, 0.8))
    plt.xlim(-0.5,492)
    # plt.ylim(0,100)
    plt.title('annual water gap occurences')
    
        
    return fig
fig = fig_7()
plt.savefig(f"{figure_path}\\WG_stripes_02_SSP3.png", dpi=200)








