# -*- coding: utf-8 -*-
"""
Created on Sat Dec  2 12:57:12 2023

@author: emidioho
"""

##### BRIDGE project. Read and plot maps of water risk and other parameters

#################################
import matplotlib
from matplotlib import pyplot as plt
plt.style.use('seaborn-v0_8-ticks') #plt.style.available[24], ggplot, seaborn-v0_8-whitegrid, seaborn-v0_8-ticks
from matplotlib.colors import Normalize

import geopandas as gpd

# import xarray as xr
import numpy as np
# import pandas as pd

import contextily as ctx

from GN_functions import get_super

##################################################################


data_path = "C:\Users\emidioho\OneDrive - Stichting Deltares\Documents\Projects\Bridge"


#%%# open raster file
# # with xr.open_dataarray(f'{data_path}\surfaceWaterAbstraction_monthTot_output.nc') as f:
# #     swa = f
# #     swa = swa.rio.write_crs('EPSG:4326', inplace=True)
    
# with xr.open_dataarray(f'{data_path}\Bridge_sharepoint\gwRecharge_monthTot_output.nc') as f:
#     swa = f
#     swa = swa.rio.write_crs('EPSG:4326', inplace=True)    

# swa = swa.rename({'lat':'x', 'lon':'y'})                             
# swa_2d = swa.drop(["time"])

# # coords = imod.util._xycoords(
# #     [float(min(swa.x)), float(max(swa.x)), 
# #      float(max(swa.y)), float(min(swa.y))
# #     ], 
# #     [0.1, 0.1]
# # )

# # dx_x = abs(np.diff(swa['x']))
# # dx = arr.array('f', dx_x)
# # dx.append(dx_x.mean()); dx=np.array(dx)

# # dy_y = abs(np.diff(swa['y']))
# # dy =  arr.array('f', dy_y)
# # dy.append(dy_y.mean()); dy=np.array(dy)

# # swa = swa.assign_coords({
# #     "dx": ('dx', dx),
# #     "dy": ('dy', dy),
# # }) 
    
# # imod.prepare.reproject(swa.drop(["time"]), like=coords, method='nearest')


##################################################################
#%%# load shapefiles

areas_risk = gpd.read_file(r'{data_path}\Bridge_sharepoint\shp\areas_risks.shp')   
# areas_SEP = gpd.read_file(f'{data_path}\Bridge_sharepoint\shp\areas_SEP.shp')
# points_SP =  gpd.read_file(f'{data_path}\test.shp')   

## SSP1 scenarios
areas_risk_SSP1 = gpd.read_file(r'{data_path}\Bridge_sharepoint\shp\areas_risks_SSP1.shp')   
## SSP3 scenarios
areas_risk_SSP3 = gpd.read_file(r'{data_path}\Bridge_sharepoint\shp\areas_risks_SSP3.shp')   



#%% #################################################################      - IGNORE PCRGLOBWB RASTER FOR NOW
### Period or specific date to plot RASTER?  (1985 - 2010)
# year_to_plot= 2000
# date_to_plot = "2000-09-22"



# year_n = year_to_plot+1
# year_p = year_to_plot-1
# summer_period = pd.date_range(start=(f'Dec-01-{year_to_plot}'), end=(f'Apr-01-{year_n}'), freq='M')
# winter_period = pd.date_range(start=(f'Jun-01-{year_to_plot}'), end=(f'Sep-01-{year_n}'), freq='M')
# wet_period = pd.date_range(start=(f'Oct-01-{year_to_plot}'), end=(f'Apr-01-{year_n}'), freq='M')
# dry_period = pd.date_range(start=(f'Apr-01-{year_to_plot}'), end=(f'Oct-01-{year_to_plot}'), freq='M')
# period_list = [summer_period, winter_period, wet_period, dry_period]


# ### Plot period?
# plot_period = True    

# ## if period_selection = True, select: summer_period, winter_period, wet_period, dry_period, 
# ## or period_list (for plotting all)
# period_ = period_list
# # period_ = summer_period





#%%# selecting only one time to plot (or a slice if a period is desired)   - IGNORE PCRGLOBWB RASTER FOR NOW
# if plot_period == True:
#     if period_ == period_list:
#         swa_data = []
#         fig, ax = plt.subplots( figsize=(8, 5),ncols=np.size(np.asarray(period_, dtype="object")))
#         fig.subplots_adjust(left=0.2, right=0.9, bottom=0.15, top=0.9, wspace=0.8)
#         for i in range(np.size(np.asarray(period_, dtype="object"))):
#             # fig, ax = plt.subplots(figsize=(8, 5))
#             ax = plt.subplot(int(f'1{i+1}1'))
#             swa_data = swa.sel( time=slice(period_[i][0], period_[i][-1]) ).mean(dim='time')
#             minv = np.nanmin(swa.sel(time=slice(swa.time[0], swa.time[-1])).min(dim="time"))
#             maxv = np.nanmax(swa.sel(time=slice(swa.time[0], swa.time[-1])).max(dim="time"))
#             levels = np.arange(minv, maxv, round(abs(minv/maxv)/5,3))
#             #naming plots
#             if i ==0:
#                 swa_data.attrs['time']='summer time'
#             if i ==1:
#                 swa_data.attrs['time']='winter time'
#             if i ==2:
#                 swa_data.attrs['time']='wet period'
#             if i ==3:
#                 swa_data.attrs['time']='dry period'
                
#             swa_data.plot(cmap='RdYlGn', levels=levels,  ax=ax)
#             plt.title(f"{swa_data.attrs['time']} - {year_to_plot}")
#             areas_risk.plot(ax=ax, edgecolor='black', color='none', linewidth=0.5)
#             ax.set_xlim([min(), max()])
#             ax.set_ylim([min(), max()])
#             # plt.show()
            
#     else:  
#      swa_data = swa.sel( time=slice(np.datetime64(period_[0]), np.datetime64(period_[-1])) ).mean(dim='time')
#      # swa_data = swa.sel(time=slice('2009-12-31','2009-03-31'))
#      # maxv= np.nanmax(swa_data.values)
#      # minv= np.nanmin(swa_data.values)
    
    
    
# else:
#     swa_data = swa.sel(time=date_to_plot, method='nearest')
#     # maxv= np.nanmax(swa_data.values)
#     # minv= np.nanmin(swa_data.values)



##################################################################
#%% prepare plot of SHAPEFILE

data = areas_risk_SSP3

## if desired, user selection can be done by columns' names
# data = data.filter(items=['SSP1_201_4','SSP1_202_4','SSP1_204_4','geometry'])

#%%############################################### LIST for plots' titles
## preparation list with scenarios names

# if 'ID_0' in str(data.columns[0]):
data_cols = data.columns
if np.size(data)>160:
    data_cols = data_cols.delete([np.arange(0,11,1)])
data_cols = data_cols.delete([-1])
list_cols = list(data_cols)

for i in range(np.size(data_cols)):  
    list_cols[i] = list_cols[i].replace("201_", "2015_")
    list_cols[i] = list_cols[i].replace("202_", "2025_")
    list_cols[i] = list_cols[i].replace("203_", "2035_")
    list_cols[i] = list_cols[i].replace("204_", "2045_")
    
    list_cols[i] = list_cols[i].replace("5_1", "5 - non poor")
    list_cols[i] = list_cols[i].replace("5_2", "5 - small farms")
    list_cols[i] = list_cols[i].replace("5_3", "5 - large farms")
    list_cols[i] = list_cols[i].replace("5_4", "5 - industry")
    list_cols[i] = list_cols[i].replace("5_5", "5 - nature")
    list_cols[i] = list_cols[i].replace("5_", "5 - poor")
        
    values = np.array(data[data_cols])
    values = values.reshape(np.size(values),1)

#%%########################### USER groups

## define user groups
poor = []           # poor
non_poor = []       # non_poor
sm_farm = []        # small_farms
lg_farm = []        # large_farms
indust = []         # industry
natur = []          # nature

## Adjusting plotting groups
for i in range(np.size(data_cols)):
    if data_cols[i].endswith("_"):
        poor.append(i)
    elif data_cols[i].endswith("_1"):
        non_poor.append(i)        
    elif data_cols[i].endswith("_2"):
        sm_farm.append(i) 
    elif data_cols[i].endswith("_3"):
        lg_farm.append(i) 
    elif data_cols[i].endswith("_4"):
        indust.append(i) 
    elif data_cols[i].endswith("_5"):
        natur.append(i)



#%%#################  NORMALIZATION
## normalization of values to [0-1]
normalize_or_not = True

############################# Normalization
if normalize_or_not == True:
    z=[]
    for v in values:
        z.append( (v-min(values))/(max(values)-min(values)) )
    norm = Normalize(min(z), max(z))
    # norm = Normalize(values.min(), values.max())
    #or without normalizing
elif  normalize_or_not == False:  
    norm = Normalize(values.min(), values.max())


matplotlib.use('Qt5Agg')  # show plots
# matplotlib.use('Agg')     # dont show plots
cmap= 'RdYlGn_r'       
 
#%%###############################################       PLOTTING 

#################################################
## USER
user_ = poor  # select user group to plot (non_poor, poor, sm_farm, lg_farm, indust, natur)

#################################################
# for i in range(np.size(data_cols)):
for i in user_:   # selecting SSP's scenarios and years (2015, 2025, 2045)
    print(i)
    fig, ax = plt.subplots(figsize=(12, 5),num=i, clear=True)
    
    # swa_data.plot.contourf(cmap='viridis', ax=ax, levels=6)   # if raster is to be plotted underneath the shape
    # norm = Normalize(vmin=min(data[data_cols[i]]), vmax=max(data[data_cols[i]]),)
    data.plot(data_cols[i],  edgecolor='k', linewidth=0.25, cmap=cmap, legend=False, ax=ax, alpha=0.7)  
    # points_SP.plot(linewidth=0.25, ax=ax)  
    ctx.add_basemap(ax, zoom=9, crs='EPSG:4326', alpha=0.7, source="CartoDB.Positron") ## add a basemap ("CartoDB.Voyager", "CartoDB.Positron")
    
    cbar = plt.cm.ScalarMappable(norm=norm, cmap=cmap)
    ax_cbar = fig.colorbar(cbar, ax=ax, alpha=0.8)   # add colorbar 
    ax_cbar.set_label('water risk  [-]', labelpad=10.0)   # add label for the colorbar
    
    plt.xlabel(f"Longitude [{get_super(' o')}]", labelpad=10.0)
    plt.ylabel(f"Latitude [{get_super(' o')}]", labelpad=10.0)
    plt.title(list_cols[i].replace("_"," "))
    plt.savefig(f"{data_path}\figures\{list_cols[i]}.png",dpi=200)
    # plt.close()
    
    
######################################################################################################################################################     
######################################################################################################################################################  
#%%###########################################################################       Municipal analysis
## Municipal analysis

data = areas_risk

munic = {'Municip': [],
         'Mean_Max_ratio': [],
         'Mean_risk': [],
         'Max_risk': [],
         'Min_risk': [],
         'Std_risk': [],
         'Median_risk': [],
         }

for i in data.NAME_2:
    munic_array = []
    munic['Municip'].extend([i])
    munic_array =  np.array(data[data.NAME_2.str.contains(i)])
    munic['Mean_Max_ratio'].extend([ np.nanmean(munic_array[0,11:46])/np.max(munic_array[0,11:46]) ] )
    munic['Mean_risk'].extend([ np.nanmean(munic_array[0,11:46]) ] )
    munic['Max_risk'].extend([ np.max(munic_array[0,11:46]) ] )
    munic['Min_risk'].extend([ np.nanmin(munic_array[0,11:46]) ] )
    munic['Std_risk'].extend([ np.nanstd(munic_array[0,11:46]) ] )
    munic['Median_risk'].extend([ np.nanmedian(munic_array[0,11:46]) ] )
       
    # print(i)
    # A = np.array(munic[ii][1].iloc[0])
    # A = A[11:46]
    # mm_ratio.append(A.mean()/A.max())
    


## assign new columns to DataFrame
areas_risk = areas_risk.assign(ratio = munic['Mean_Max_ratio'])
areas_risk = areas_risk.assign(mean_risk = munic['Mean_risk'])
areas_risk = areas_risk.assign(max_risk = munic['Max_risk'])
areas_risk = areas_risk.assign(min_risk = munic['Min_risk'])
areas_risk = areas_risk.assign(std_risk = munic['Std_risk'])
areas_risk = areas_risk.assign(med_risk = munic['Median_risk'])


## areas where [value]<[mean(threshold)]
# areas_risk_bmean = areas_risk.where(areas_risk.ratio>=np.mean(areas_risk.ratio))
# areas_risk_min = areas_risk.where(areas_risk.min_risk<np.mean(areas_risk.min_risk))


#%%###########################################################################             RISK Ratio     plot
fig, ax = plt.subplots(figsize=(18, 6),num=10, clear=True, nrows= 1, ncols=2, gridspec_kw={'width_ratios': [3, 1]})      
alpha= 0.8
cm = matplotlib.colormaps[cmap]
norm = plt.Normalize(vmin=min(areas_risk.ratio), vmax=max(areas_risk.ratio))
plt.subplots_adjust(left=0.1, right=0.95, bottom=0.15, top=0.9, wspace=0.2, hspace=0.2)
       
ax1 = plt.subplot(121)
areas_risk.plot(column = 'ratio',  edgecolor='k', linewidth=0.25, cmap=cmap, legend=False, ax=ax1, alpha=alpha)                

ctx.add_basemap(ax1, zoom=9, crs='EPSG:4326', alpha=alpha, source="CartoDB.Positron") ## add a basemap ("CartoDB.Voyager", "CartoDB.Positron")

cbar = plt.cm.ScalarMappable(norm=norm, cmap=cmap)    
ax_cbar = fig.colorbar(cbar, ax=ax1, alpha=0.8)   # add colorbar 
ax_cbar.set_label('risk ratio [-]', labelpad=10.0)   # add label for the colorbar
#
plt.xlabel(f"Longitude [{get_super(' o')}]", labelpad=10.0)
plt.ylabel(f"Latitude [{get_super(' o')}]", labelpad=10.0)
plt.title('Risk ratio = [mean(water_risk) ÷ max(water_risk)]', pad=10)

#########
ax2 = plt.subplot(122)
n, bins, patches = plt.hist(areas_risk.ratio, bins = 20,  density=False,
                            edgecolor='k', linewidth=0.2, facecolor='grey',
                            orientation='vertical', align='mid')
bin_centers = 0.5 * (bins[:-1] + bins[1:])
col = bin_centers - min(bin_centers)
col /= max(col)
for c, p in zip(col, patches):
    plt.setp(p, 'facecolor', cm(c), alpha=alpha)

plt.plot([np.nanmedian(areas_risk.ratio), np.nanmedian(areas_risk.ratio)],[-0.1, 12],
          linewidth=2, color='c')
plt.text(np.nanmedian(areas_risk.ratio)-0.016, 5., f'median risk ratio = {np.round(np.nanmedian(areas_risk.ratio),2)}', rotation=90, color='c')
#
plt.ylim([0,10.01])
plt.xlabel('risk ratio [-]', labelpad=10.0)
plt.ylabel('frequency [-]', labelpad=10.0)
#
plt.show()
# plt.savefig(f"{data_path}\figures\risk_ratio_.png",dpi=200)


#%%###################################           median and Max RISK      plot
fig, ax = plt.subplots(figsize=(28,6),num=20, clear=True, nrows= 1, ncols=2, gridspec_kw={'width_ratios': [1, 1]})      
# alpha= 0.8
# cm = matplotlib.colormaps[cmap]
plt.subplots_adjust(left=0.1, right=0.95, bottom=0.15, top=0.9, wspace=0.2, hspace=0.2)
       
ax1 = plt.subplot(121)
areas_risk.plot(column = 'med_risk',  edgecolor='k', linewidth=0.25, cmap=cmap, legend=False, ax=ax1, alpha=alpha)                

ctx.add_basemap(ax1, zoom=9, crs='EPSG:4326', alpha=alpha, source="CartoDB.Positron") ## add a basemap ("CartoDB.Voyager", "CartoDB.Positron")

norm = plt.Normalize(vmin=min(areas_risk.med_risk), vmax=max(areas_risk.med_risk))
cbar = plt.cm.ScalarMappable(norm=norm, cmap=cmap)    
ax_cbar = fig.colorbar(cbar, ax=ax1, alpha=alpha)   # add colorbar 
ax_cbar.set_label('median water risk [-]', labelpad=10.0)   # add label for the colorbar
#
plt.xlabel(f"Longitude [{get_super(' o')}]", labelpad=10.0)
plt.ylabel(f"Latitude [{get_super(' o')}]", labelpad=10.0)
plt.title('Median water risk', pad=10)

#########
ax2 = plt.subplot(122)
areas_risk.plot(column = 'max_risk',  edgecolor='k', linewidth=0.25, cmap=cmap, legend=False, ax=ax2, alpha=alpha)                

ctx.add_basemap(ax2, zoom=9, crs='EPSG:4326', alpha=alpha, source="CartoDB.Positron") ## add a basemap ("CartoDB.Voyager", "CartoDB.Positron")

norm = plt.Normalize(vmin=min(areas_risk.max_risk), vmax=max(areas_risk.max_risk))
cbar = plt.cm.ScalarMappable(norm=norm, cmap=cmap)    
ax_cbar = fig.colorbar(cbar, ax=ax2, alpha=0.8)   # add colorbar 
ax_cbar.set_label('max. water risk [-]', labelpad=10.0)   # add label for the colorbar
#
plt.xlabel(f"Longitude [{get_super(' o')}]", labelpad=10.0)
plt.ylabel(f"Latitude [{get_super(' o')}]", labelpad=10.0)
plt.title('Maximum water risk', pad=10)
#
plt.show()
# plt.savefig(f"{data_path}\figures\median_&_max_risk_.png",dpi=300)
         


#%%#################################        AREAS with Minimum risk         plot
fig, ax = plt.subplots(figsize=(18, 6), num=30, clear=True, nrows= 1, ncols=2, gridspec_kw={'width_ratios': [3, 1]})
norm = plt.Normalize(vmin=min(areas_risk.min_risk), vmax=max(areas_risk.min_risk))
plt.subplots_adjust(left=0.1, right=0.95, bottom=0.15, top=0.9, wspace=0.2, hspace=0.2)
       
ax1 = plt.subplot(121)
# areas_risk.plot(column = 'NAME_0',  edgecolor='k', linewidth=0.25, color='k', legend=False, ax=ax1, alpha=alpha)               
areas_risk.plot(column = 'min_risk',  edgecolor='k', linewidth=0.25, cmap=cmap, legend=False, ax=ax1, alpha=alpha)      


ctx.add_basemap(ax1, zoom=9, crs='EPSG:4326', alpha=alpha, source="CartoDB.Positron") ## add a basemap ("CartoDB.Voyager", "CartoDB.Positron")

cbar = plt.cm.ScalarMappable(norm=norm, cmap=cmap)    
ax_cbar = fig.colorbar(cbar, ax=ax1, alpha=alpha)   # add colorbar 
ax_cbar.set_label('min. water risk[-]', labelpad=10.0)   # add label for the colorbar
#
plt.xlabel(f"Longitude [{get_super(' o')}]", labelpad=10.0)
plt.ylabel(f"Latitude [{get_super(' o')}]", labelpad=10.0)
plt.title('Minimum water risk', pad=10)

#########
ax2 = plt.subplot(122)
n, bins, patches = plt.hist(areas_risk.min_risk, bins = 20,  density=False,
                            edgecolor='k', linewidth=0.2, facecolor='grey',
                            orientation='vertical', align='mid')
bin_centers = 0.5 * (bins[:-1] + bins[1:])
col = bin_centers - min(bin_centers)
col /= max(col)
for c, p in zip(col, patches):
    plt.setp(p, 'facecolor', cm(c), alpha=alpha)

plt.plot([np.nanmedian(areas_risk.min_risk), np.nanmedian(areas_risk.min_risk)],[-0.1, 25],
         linewidth=2, color='c')
plt.text(np.nanmedian(areas_risk.min_risk)+0.0003, 5., f'median  min. risk = {np.round(np.nanmedian(areas_risk.min_risk),4)}', rotation=90, color='c')
#
plt.ylim([0,25])
plt.xlabel('min. water risk [-]', labelpad=10.0)
plt.ylabel('frequency [-]', labelpad=10.0)
#
plt.show()
# plt.savefig(f"{data_path}\figures\min_risk.png",dpi=200)


#%%#################################        STD risk         plot
fig, ax = plt.subplots(figsize=(18, 6), num=40, clear=True, nrows= 1, ncols=2, gridspec_kw={'width_ratios': [3, 1]})
norm = plt.Normalize(vmin=min(areas_risk.std_risk), vmax=max(areas_risk.std_risk))
plt.subplots_adjust(left=0.1, right=0.95, bottom=0.15, top=0.9, wspace=0.2, hspace=0.2)
       
ax1 = plt.subplot(121)
# areas_risk.plot(column = 'NAME_0',  edgecolor='k', linewidth=0.25, color='k', legend=False, ax=ax1, alpha=alpha)               
areas_risk.plot(column = 'std_risk',  edgecolor='k', linewidth=0.25, cmap=cmap, legend=False, ax=ax1, alpha=alpha)      


ctx.add_basemap(ax1, zoom=9, crs='EPSG:4326', alpha=alpha, source="CartoDB.Positron") ## add a basemap ("CartoDB.Voyager", "CartoDB.Positron")

cbar = plt.cm.ScalarMappable(norm=norm, cmap=cmap)    
ax_cbar = fig.colorbar(cbar, ax=ax1, alpha=alpha)   # add colorbar 
ax_cbar.set_label('std.d.  water risk[-]', labelpad=10.0)   # add label for the colorbar
#
plt.xlabel(f"Longitude [{get_super(' o')}]", labelpad=10.0)
plt.ylabel(f"Latitude [{get_super(' o')}]", labelpad=10.0)
plt.title('Standard dev.  water risk', pad=10)

#########
ax2 = plt.subplot(122)
n, bins, patches = plt.hist(areas_risk.std_risk, bins = 20,  density=False,
                            edgecolor='k', linewidth=0.2, facecolor='grey',
                            orientation='vertical', align='mid')
bin_centers = 0.5 * (bins[:-1] + bins[1:])
col = bin_centers - min(bin_centers)
col /= max(col)
for c, p in zip(col, patches):
    plt.setp(p, 'facecolor', cm(c), alpha=alpha)

plt.plot([np.nanmedian(areas_risk.std_risk), np.nanmedian(areas_risk.std_risk)],[-0.1, 25],
         linewidth=2, color='c')
plt.text(np.nanmedian(areas_risk.std_risk)-0.0025, 1, f'median  std.d. risk = {np.round(np.nanmedian(areas_risk.std_risk),3)}', rotation=90, color='c')
#
plt.ylim([0,15])
plt.xlabel('std.d. water risk [-]', labelpad=10.0)
plt.ylabel('frequency [-]', labelpad=10.0)
#
plt.show()
# plt.savefig(f"{data_path}\figures\std_risk.png",dpi=200)


#%%#######################        TRASH





# colors = "turbo"
# levels = np.arange(minv,maxv,0.05)

# fig = plt.figure(num=1, clear = True)
# # ax1 = plt.subplot(111)

# # ax1 = swa_data.plot(cmap='RdYlGn_r')
# # ax1.axes.set_aspect('equal')
# areas_risk.plot(ax=ax1)

# # ax1.imshow(swa_data, cmap='turbo', alpha=0.9) #Set3, tab10, turbo
# # imod.visualize.plot_map(swa_data, colors, levels)

# # areas_risk.plot()

# plt.xlabel(f"latitude {get_super('o')}")
# plt.ylabel(f"longitude {get_super('o')}")
# plt.show()




#%

# fig, ax = plt.subplots(figsize=(8, 5))
# # swa_data.plot(cmap='RdYlGn', ax=ax)
# swa_data.plot.contourf(cmap='RdYlGn', ax=ax, levels=6)
# plt.title("")
# areas_risk.plot(ax=ax, edgecolor='black', color='none', linewidth=0.5)
# ax.set_xlim(-95, -87)
# ax.set_ylim(26, 32)




#%

# import fiona
# import rasterio
# import rasterio.mask

# with fiona.open(f'{data_path}\shp\areas_risks.shp', "r") as shapefile:
#     shapes = [feature["geometry"] for feature in shapefile]
 
 
# with rasterio.open(f'{data_path}\gwRecharge_monthTot_output.nc') as src:
#     out_image, out_transform = rasterio.mask.mask(src, shapes, nodata=np.nan, crop=True)
#     out_meta = src.meta    
 
 
 
# out_meta.update({"driver": "GTiff",
#                  "height": out_image.shape[1],
#                  "width": out_image.shape[2],
#                  "transform": out_transform})

# with rasterio.open("RGB.byte.masked.tif", "w", **out_meta) as dest:
#     dest.write(out_image)
 
 
 # data.plot(data_cols[i],  edgecolor='black', linewidth=0.25, cmap=cmap, legend=False, ax=ax, alpha=0.95)  