# -*- coding: utf-8 -*-
"""
Created on Fri Jan  5 17:18:10 2024

@author: emidioho
"""


import pandas as pd
import numpy as np
import geopandas as gpd
import xarray as xr

import contextily as ctx

import seaborn as sns
from matplotlib import pyplot as plt
from matplotlib.colors import Normalize
plt.style.use('seaborn-v0_8-ticks') #plt.style.available[24], ggplot, seaborn-v0_8-whitegrid, seaborn-v0_8-ticks


#%% WGI calculation (from _calculate_WGI - GitLab)
def count_sequences(sequence, treshhold):
    counts = []
    for j in range(np.size(sequence,0)):
        count = 0
        counted = False
        for i in sequence.iloc[j]:
            # print(i)
            if i > treshhold and not counted:
                count = count+1
                counted = True
            if i <= treshhold:
                counted = False
        counts.append(count)    
    return counts


def get_max_of_sequences(sequence, treshhold):
    maxs_all =[]
    for j in range(np.size(sequence,0)):
        # print(j)
        current_sequence = []
        maxs = []
        for i in range(np.size(sequence.iloc[j])):
            # print(sequence[j,i])
            if sequence.iloc[j,i] > treshhold:
                current_sequence.append(sequence.iloc[j,i])
            if sequence.iloc[j,i] <= treshhold and len(current_sequence)>0:
                maxs.append(max(current_sequence))
                current_sequence = []
        if len(current_sequence)>0:
           maxs.append(max(current_sequence))
        try:
            maxs_all.append(max(maxs))
        except:
            maxs_all.append(0)
    return maxs_all


#%% folder paths to save some files
figure_path = r"C:\Users\emidioho\OneDrive - Stichting Deltares\Documents\Projects\Bridge\figures"
folder_path = r"C:\Users\emidioho\OneDrive - Stichting Deltares\Documents\Projects\Bridge"
#### read shapefile (only used for plotting, so it does not matter the SSP case)
data_shape = gpd.read_file(r'N:\Projects\1230000\1230409\B. Measurements and calculations\Risk Calculations\input\shp/areas_risks_SSP3.shp')   
names = data_shape.NAME_2

#companies data:
data_comp = pd.read_csv(r'N:\Projects\1230000\1230409\B. Measurements and calculations\Risk Calculations\input\Sao_Paulo_HydroBASIN_Level6_AssetLevelData.csv', sep=';')
data_comp = data_comp.dropna()
data_comp = gpd.GeoDataFrame(data_comp, geometry=gpd.points_from_xy(data_comp.x, data_comp.y), crs="EPSG:4326")


#%%###################################################################################
scenario = 'SSP1'   # either SSP1 or SSP3

       
#%% Demand and availability files from N drive
demand = pd.read_csv(f'N:/Projects/1230000/1230409/B. Measurements and calculations/6. Sites in Brazil/risk2/water/demandTotalForcastMonth_{scenario}.csv')
demand = demand[demand.columns[1:]]

availability = pd.read_csv(f'N:/Projects/1230000/1230409/B. Measurements and calculations/6. Sites in Brazil/risk2/water/availability_{scenario}.csv')
availability = availability[availability.columns[1:np.size(demand,1)+1]]


#%%## WGI
gap = demand - availability

count_months = np.size(gap,1)
count_months_target_met = np.sum(gap<=0,1)
count_months_target_not_met = count_months - count_months_target_met
count_failures = count_sequences(gap,0)
# sum_max_gap = np.sum(get_max_of_sequences(gap/demand,0))
sum_max_gap = get_max_of_sequences(gap/demand,0)

#print("count_months", count_months)
#print("count_months_target_met", count_months_target_met)
#print("count_months_target_not_met", count_months_target_not_met)
#print("count_failures", count_failures)
#print("sum_max_gap", sum_max_gap)

# reliability aka frequency
# 0 = lowest hazard = no gaps
# 1 = highest hazard = always a gap
reliability = count_months_target_not_met / count_months

# resilience aka persistence
# 0 = lowest hazard = gaps are short and are scattered over the whole time series
# 1 = highest hazard = there is one, very long, gap
resilience = []
for i in range(np.size(count_months_target_not_met)):
    if count_months_target_not_met[i] != 0:
        resilience.append(1 - (count_failures[i] / count_months_target_not_met[i]))
    else:
        resilience.append(0) #0=lowest hazard

# vulnerability aka severity
# 0 = lowest hazard = gaps are small, low% of demand
# 1 = highest hazard = gaps are large, hight% of demand
vulnerability = []
for i in range(np.size(count_failures)):
    if count_failures[i] != 0:
        vulnerability.append(sum_max_gap[i] / count_failures[i])
    else:
        vulnerability.append(0) #0=lowest risk


WGI = (reliability + resilience + vulnerability)/3

#%%# merging data back to shapefile
data_shape = data_shape.assign(reliability = reliability)
data_shape = data_shape.assign(resilience = resilience)
data_shape = data_shape.assign(vulnerability = vulnerability)

data_shape = data_shape.assign(WGI = WGI)

# data_shape.to_file(f'{folder_path}/WGI_SP_{scenario}.shp', driver='ESRI Shapefile')


#%% total demand per user
user = 'Agri','Domestic','Industry','Irrigation','Livestock'
i=-1
for user_i in user:
    i+=1
    demand_user = pd.read_csv(f'N:/Projects/1230000/1230409/B. Measurements and calculations/6. Sites in Brazil/risk2/water/demand{user_i}ForcastMonth_{scenario}.csv')
    demand_user.drop('Unnamed: 0', axis=1, inplace=True)
    if i==0:
        da_user = xr.DataArray(demand_user)
        da_user=da_user.expand_dims(user = [user_i])
    else:
        demand_user = xr.DataArray(demand_user)
        demand_user = demand_user.expand_dims(user = [user_i])
        
        da_user= xr.concat([da_user, demand_user],dim='user')


da_tot = np.nansum(da_user.sum(dim='user'),0)
x=np.array(da_user.dim_1.values.astype(int))
y1= np.array( np.nansum(np.nansum(da_user.where(da_user.user=='Agri'),1),0))
y2=  np.array( np.nansum(np.nansum(da_user.where(da_user.user=='Domestic'),1),0))
y3=  np.array( np.nansum(np.nansum(da_user.where(da_user.user=='Industry'),1),0))
y4=  np.array( np.nansum(np.nansum(da_user.where(da_user.user=='Irrigation'),1),0))
y5= np.array( np.nansum(np.nansum(da_user.where(da_user.user=='Livestock'),1),0))





####################################################################################
####################################################################################
#%% FIGURES and PLOTS

### figure elements
months = np.linspace(1,12,12)
cmap = 'RdYlGn_r'
alpha = 0.9


#%% fig tot_demand over time
fig,ax = plt.subplots(2,1, figsize=(12,8), num=220, clear=True)    
plt.subplots_adjust(left=0.10, right=0.90, bottom=0.1, top=0.95, wspace=0.05, hspace=0.35)

ax1 = plt.subplot(211)
plt.stackplot(x, (y1/da_tot)*100, (y2/da_tot)*100, (y3/da_tot)*100, (y4/da_tot)*100, linewidth=0.25, edgecolor='k')
plt.xticks(np.arange(0,492,492/42+1).astype(int), (np.arange(0,492,492/42+1)/12).astype(int)+2000, rotation=45, color=(0.5, 0.5, 0.5))
plt.xlim(0,492)
plt.ylabel('percentage of demand')
plt.legend(['Agri','Domestic','Industry','Irrigation'], frameon=True, edgecolor='k', ncols=5, loc=9) 
 
ax2 = plt.subplot(212)
plt.plot(x, y1)
plt.plot(x, y2)
plt.plot(x, y3)
plt.plot(x, y4)
plt.plot(x,np.nansum(availability,0),'k--')
plt.xticks(np.arange(0,492,492/42+1).astype(int), (np.arange(0,492,492/42+1)/12).astype(int)+2000, rotation=45, color=(0.5, 0.5, 0.5))
plt.xlim(0,492)
plt.legend(['Agri','Domestic','Industry','Irrigation','Availability'], frameon=True, edgecolor='k', ncols=5, loc=9)  

# plt.savefig(f"{figure_path}\\tot_demand_vs_avail_SP_{scenario}.png", dpi=300)


#%% map with names and companies count

#merging polygons and industries points (industry per district)
dfsjoin = gpd.sjoin(data_shape, data_comp) #Spatial join Points to polygons
dfpivot = pd.pivot_table(dfsjoin,index='areaid',columns='indstry',aggfunc={'indstry':len})
dfpivot.columns = dfpivot.columns.droplevel()
data_shape_ind = data_shape.merge(dfpivot, how='left', on='areaid')

# data_shape_ind.iloc[:,30:] = data_shape_ind.iloc[:,30:].where(~np.isnan(data_shape_ind.iloc[:,30:].values),0)

data_shape_n = data_shape
data_shape_n['coords'] = data_shape['geometry'].apply(lambda x: x.representative_point().coords[:])
data_shape_n['coords'] = [coords[0] for coords in data_shape_n['coords']]
data_shape_n['tot_ind'] = np.nansum(data_shape_ind.iloc[:,30:],1)

fig, ax = plt.subplots(figsize=(12,8), num=450, clear=True)
plt.subplots_adjust(left=0.15, right=0.9, bottom=0.15, top=0.95, wspace=0.2, hspace=0.3)

data_shape_n.plot(column = data_shape_n.tot_ind, edgecolor='k', linewidth=0.25, legend=False, ax=ax, cmap=cmap)
ctx.add_basemap( ax=ax, zoom=9, crs='EPSG:4326', alpha=0.7, source="CartoDB.Positron") ## add a basemap ("CartoDB.Voyager", "CartoDB.Positron")
for idx, row in data_shape_n.iterrows():
    plt.annotate(f"{row['NAME_2']}\n {int(row['tot_ind'])}", xy=row['coords'], horizontalalignment='center',size=7, color='w')
    
del data_shape_n

# plt.savefig(f"{figure_path}\\SP_map_industries_count.png", dpi=300)

#%% RRV maps
norm = Normalize(0, 1)
fig, ax = plt.subplots(3,1, figsize=(7,8), num=11, clear=True)
plt.subplots_adjust(left=0.25, right=0.90, bottom=0.1, top=0.95, wspace=0.25, hspace=0.15)

ax = plt.subplot(311)
data_shape.plot(column = 'reliability',  edgecolor='k', linewidth=0.25, norm=norm, cmap=cmap, legend=False, ax=ax)
ctx.add_basemap(ax, zoom=9, crs='EPSG:4326', alpha=0.7, source="CartoDB.Positron") ## add a basemap ("CartoDB.Voyager", "CartoDB.Positron")
cbar = plt.cm.ScalarMappable(norm=norm, cmap=cmap)
ax_cbar = fig.colorbar(cbar, ax=ax, shrink =0.95)  # add colorbar 
ax_cbar.set_ticks(np.linspace(0,1,2))
ax_cbar.set_ticklabels(['few gaps', 'many gaps'])
plt.ylabel('reliability\n(frequency)', labelpad=20)   # add label for the colorbar
plt.xticks([],'')
# plt.title('reliability')

ax = plt.subplot(312)
data_shape.plot(column = 'resilience',  edgecolor='k', linewidth=0.25, norm=norm, cmap=cmap, legend=False, ax=ax)
ctx.add_basemap(ax, zoom=9, crs='EPSG:4326', alpha=0.7, source="CartoDB.Positron") ## add a basemap ("CartoDB.Voyager", "CartoDB.Positron")
cbar = plt.cm.ScalarMappable(norm=norm, cmap=cmap)
ax_cbar = fig.colorbar(cbar, ax=ax, shrink =0.95)  # add colorbar 
ax_cbar.set_ticks(np.linspace(0,1,2))
ax_cbar.set_ticklabels(['short gaps', 'long gaps'])
plt.ylabel('resilience\n(persistence)', labelpad=20)   # add label for the colorbar
plt.xticks([],'')
# plt.title('resilience')

ax = plt.subplot(313)
data_shape.plot(column = 'vulnerability',  edgecolor='k', linewidth=0.25, norm=norm, cmap=cmap, legend=False, ax=ax)
ctx.add_basemap(ax, zoom=9, crs='EPSG:4326', alpha=0.7, source="CartoDB.Positron") ## add a basemap ("CartoDB.Voyager", "CartoDB.Positron")
cbar = plt.cm.ScalarMappable(norm=norm, cmap=cmap)
ax_cbar = fig.colorbar(cbar, ax=ax, shrink =0.95)  # add colorbar 
ax_cbar.set_ticks(np.linspace(0,1,2))
ax_cbar.set_ticklabels(['small gaps', 'large gaps'])
plt.ylabel('vulnerability\n(severity)', labelpad=20)   # add label for the colorbar
# plt.title('vulnerability')
    
# plt.savefig(f"{figure_path}\\RRV_SP_{scenario}.png", dpi=300)



#%% map WGI
fig, ax = plt.subplots(figsize=(10,8), num=161, clear=True)
plt.subplots_adjust(left=0.15, right=0.95, bottom=0.1, top=0.95, wspace=0.25, hspace=0.15)

norm = Normalize(0, 1)
ax = data_shape.plot(column = 'WGI',  edgecolor='k', linewidth=0.25, norm=norm, cmap=cmap, legend=False, ax=ax, alpha=0.9)
ctx.add_basemap(ax, zoom=9, crs='EPSG:4326', alpha=0.7, source="CartoDB.Positron") ## add a basemap ("CartoDB.Voyager", "CartoDB.Positron")
cbar = plt.cm.ScalarMappable(norm=norm, cmap=cmap)
ax_cbar = fig.colorbar(cbar, ax=ax, shrink =0.75, alpha=0.9)  # add colorbar 
ax_cbar.set_ticks(np.linspace(0,1,3))
ax_cbar.set_ticklabels(['low hazard', 'med. hazard', 'high hazard'])
ax_cbar.set_label('WGi [-]', labelpad=20)   # add label for the colorbar
# plt.xticks([],'')

# plt.savefig(f"{figure_path}\\WGI_SP_{scenario}.png", dpi=300)


#%% map WGI  and INDUSTRIES
fig, ax = plt.subplots(figsize=(16,8), num=161, clear=True)
plt.subplots_adjust(left=0.15, right=0.95, bottom=0.1, top=0.95, wspace=0.25, hspace=0.15)

norm = Normalize(0, 1)
ax = data_shape.plot(column = 'WGI',  edgecolor='k', linewidth=0.25, norm=norm, cmap=cmap, legend=False, ax=ax, alpha=0.9)
ctx.add_basemap(ax, zoom=9, crs='EPSG:4326', alpha=0.7, source="CartoDB.Positron") ## add a basemap ("CartoDB.Voyager", "CartoDB.Positron")
cbar = plt.cm.ScalarMappable(norm=norm, cmap=cmap)
ax_cbar = fig.colorbar(cbar, ax=ax, shrink =0.75, alpha=0.9)  # add colorbar 
ax_cbar.set_ticks(np.linspace(0,1,3))
ax_cbar.set_ticklabels(['low hazard', 'med. hazard', 'high hazard'])
ax_cbar.set_label('WGi [-]', labelpad=20)   # add label for the colorbar


# add industries'locations
sns.scatterplot(x=data_comp['longitd'], y=data_comp['latitud'], hue= data_comp['indstry'], edgecolor='k', palette = 'Set1', style= data_comp['indstry'], legend='full')
plt.legend(title='industry type', frameon=True, edgecolor='k',loc=1, fontsize=8)

# plt.savefig(f"{figure_path}\\WGI_SP_and_Indust_{scenario}.png", dpi=300)


#%% histogram WGI

SP_SSP1 = gpd.read_file(f'{folder_path}/WGI_SP_SSP1.shp')
WGI_SP_SSP1 = SP_SSP1.WGI/max(SP_SSP1.WGI)
SP_SSP3 = gpd.read_file(f'{folder_path}/WGI_SP_SSP3.shp')
WGI_SP_SSP3 = SP_SSP3.WGI/max(SP_SSP3.WGI)

fig, ax = plt.subplots(1,2, figsize=(9,3), num=162, clear=True)
plt.subplots_adjust(left=0.15, right=0.90, bottom=0.1, top=0.90, wspace=0.45, hspace=0.15)


ax1 = plt.subplot(121)
sns.boxplot(data = WGI_SP_SSP1)
plt.title('WGI SSP1')

ax2 = plt.subplot(122)
sns.boxplot(data = WGI_SP_SSP3, color = 'orange')
plt.title('WGI SSP3')



#%% scatter plots WGI for different SSP

fig, ax = plt.subplots(1,2, figsize=(8,4), num=163, clear=True)
plt.subplots_adjust(left=0.15, right=0.95, bottom=0.15, top=0.90, wspace=0.35, hspace=0.15)

ax2 = plt.subplot(121)
plt.plot([0,1.1],[0,1.1],'--k', linewidth=0.5)
plt.scatter(WGI_SP_SSP1, WGI_SP_SSP3, s=9)
plt.xlabel('WGI SSP1')
plt.ylabel('WGI SSP3')

for i, txt in enumerate(SP_SSP1.NAME_2):
    ax2.annotate(txt, (WGI_SP_SSP1[i], WGI_SP_SSP3[i]), fontsize=5)


ax2 = plt.subplot(122)
plt.plot([0,1.1],[0,1.1],'--k', linewidth=0.5)
plt.scatter(WGI_SP_SSP1, WGI_SP_SSP3, s=9)
# plt.xlabel('WGI SSP1')
# plt.ylabel('WGI SSP3')
plt.xlim(0.95, 1.01)
plt.ylim(0.95, 1.01)
for i, txt in enumerate(SP_SSP1.NAME_2):
    ax2.annotate(txt, (WGI_SP_SSP1[i], WGI_SP_SSP3[i]), fontsize=8)
    
# plt.savefig(f"{figure_path}\\WGI_SP_comparison_scatter.png", dpi=300
    


#%% industries count plot
x_loc = np.arange(0,np.size(dfpivot,1))
names_loc = np.linspace(0+0.5,len(data_shape.NAME_2)-0.5,len(data_shape.NAME_2))

fig, ax = plt.subplots(figsize=(16,6), num=331, clear=True)
plt.subplots_adjust(left=0.40, right=0.95, bottom=0.35, top=0.95, wspace=0.2, hspace=0.3)

sns.heatmap(data_shape_ind.iloc[:,30:].transpose(), cmap=cmap, cbar=False, linewidths=0.003, linecolor='k', alpha=alpha, annot=True, fmt=".0f")
plt.yticks(x_loc+0.5, labels = list(dfpivot.columns), rotation=0)
plt.xticks(names_loc ,labels = data_shape.NAME_2, rotation=90)

# plt.savefig(f"{figure_path}\\SP_heat_map_industries_count.png", dpi=300)


# plt.close('all')

